<%-- 
    Document   : editbuku.jsp
    Created on : Jun 10, 2019, 10:46:54 PM
    Author     : abah
--%>

<%@page import="buku.Model.BukuModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Thema Content Managemen System</title>
    <link rel="stylesheet" type="text/css" href="asset/css/setone.css">
    <link rel="stylesheet" type="text/css" href="asset/css/form.css">
</head>

<body>
    <header>
        <div class="head">
            <div class="kiri-head">
                <img id="img-mobile" src="asset/images/wordmark4.png" alt="none">
            </div>
            <div class="kanan-head">
                <div class="button-div">
                    <img src="asset/images/avatar/ava2.jpg" alt="">
                    Wahyu
                </div>
            </div>
        </div>
    </header>
    <nav>
        <aside id="menu-mobile" class="menu-mobile">
            <ul>
                <li><a href="index.html">Dashboard</a></li>
                <li><a href="form.html">Transaksi</a></li>
                <li><a href="">Buku</a></li>
                <li><a href="">Pengarang</a></li>
                <li><a href="">Pengaturan</a></li>
            </ul>
        </aside>
        <aside id="menu" class="menu">
            <div class="head-img">
                <img id="head-img" src="asset/images/wordmark4.png" alt="none">
            </div>
            <ul>
                <li>
                    <a href="/BukuGlassFish/">
                        <img class="icon-menu" src="asset/images/icon/home.png" alt="none">
                        <span>Dashboard</span>
                    </a>
                </li
                <hr>
                <li>
                    <a href="/BukuGlassFish/transaksi?sort=buku">
                        <img class="icon-menu" src="asset/images/icon/apotik.png" alt="none">
                        <span>Transaksi</span>
                    </a>
                </li>
                <hr>
                <li>
                    <a href="/BukuGlassFish/buku">
                        <img class="icon-menu" src="asset/images/icon/kamar.png" alt="none">
                        <span>Buku</span>
                    </a>
                </li>
                <hr>
                <li>
                    <a href="/BukuGlassFish/pengarang">
                        <img class="icon-menu" src="asset/images/icon/petugas.png" alt="none">
                        <span>Pengarang</span>
                    </a>
                </li>
                <hr>
            </ul>
            <br>
            <br>
            <img id="button" class="panah" src="asset/images/icon/kiri.png" alt="none">
        </aside>
    </nav>
    <section>
        <article id="isi" class="isi">
            <div class="grid-container">
                <div class="kolom">
                    <div class="box">
                        <div class="box-container judul">
                            <h4><b>Halaman Judul</b></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-container">
                <div class="baris">
                    <div class="kolom">
                        <div class="box">
                            <div class="box-container judul">
                                 <%BukuModel buuk = (BukuModel) request.getAttribute("data");%>
                                <h4><b>Form Edit Data</b></h4>
                            </div>
                            <div class="box-container form text-kiri">
                                <form method="post" action="/BukuGlassFish/buku?post=edit">
                                    <input type="hidden" name="nobuku" value="<% out.print(buuk.getNoBuku()); %>">
                                    <label for="fobat">Nama Buku</label>
                                    <input type="text" id="namabuku" name="namabuku" placeholder="Nama Buku.." value="<% out.print(buuk.getNamaBuku()); %>">
                                    <label for="fobat">ISBN Buku</label>
                                    <input type="text" id="isbnbuku" name="isbnbuku" placeholder="ISBN Buku.." value="<% out.print(buuk.getIsbnBuku()); %>">
                                    <label for="ftgl">Tgl diterbitkan <span class="note"> Tanggal Min 10 Maret 2019 </span></label>
                                    <input type="date" id="tglterbit" name="tglterbit" min="2019-03-10" placeholder="Tanggal Terbit Buku.." value="<% out.print(buuk.getTglTerbit()); %>">
                                    
                                    <input type="submit" value="Submit">
                                </form>
                            </div>
                        </div>
                    </div>
<!--                    <div class="kolom">
                        <div class="box">
                            <div class="box-container judul">
                                <h4><b>Output Data Dari Form</b></h4>
                            </div>
                            <div class="box-container text-kiri">
                                <label for="foobat">Nama Obat : </label>
                                <span class="form-output"> Paracetamol </span>
                                <label for="fotglpembuatan">Tgl Pembuatan : </label>
                                <span class="form-output"> 10 Maret 2019 </span>
                                <label for="fotglkadaluwarsa">Tgl Kadaluwarsa : </label>
                                <span class="form-output"> 20 Maret 2019 </span>
                                <label for="fotel">Telephon Toko : </label>
                                <span class="form-output"> 0878 0492 2443 </span>
                                <label for="foemail">Email Toko : </label>
                                <span class="form-output"> indofarma@idf.com </span>
                                <label for="forak">Rak : </label>
                                <span class="form-output"> Rak No 1 </span>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </article>
    </section>
    <footer>
        <div id="foot" class="foot">
            <div class="identitas">
                <strong>Wahyu Setiawan || 17.5.00173</strong>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="asset/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="asset/js/sidebar.js"></script>
</body>

</html>