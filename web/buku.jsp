<%-- 
    Document   : buku
    Created on : Jun 10, 2019, 8:19:23 PM
    Author     : abah
--%>

<%@page import="buku.Model.BukuModel"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Thema Content Managemen System</title>
    <link rel="stylesheet" type="text/css" href="asset/css/setone.css">
    <!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.btnView {
  background-color: DodgerBlue;
  border: none;
  color: white;
  padding: 12px 16px;
  font-size: 16px;
  cursor: pointer;
}

/* Darker background on mouse-over */
.btnView:hover {
  background-color: RoyalBlue;
}
.btnEdit {
  background-color: #187b09;
  border: none;
  color: white;
  padding: 12px 16px;
  font-size: 16px;
  cursor: pointer;
}

/* Darker background on mouse-over */
.btnEdit:hover {
  background-color: #0f4e06;
}
.btnDelete {
  background-color: #dd415b;
  border: none;
  color: white;
  padding: 12px 16px;
  font-size: 16px;
  cursor: pointer;
}

/* Darker background on mouse-over */
.btnDelete:hover {
  background-color: #9b1c31;
}
form.example{
    padding-top: 20px;
    padding-bottom: 5px;
    float: right;
    width: 100;
    padding-right: 0.7%;
}
form.example input[type=text] {
  padding: 10px;
  font-size: 17px;
  border: 1px solid grey;
  float: left;
  width: 70%;
  background: #f1f1f1;
}
form.example button:hover{
  background: #0b7dda;
}

form.example::after {
  content: "";
  clear: both;
  display: table;
}

form.example button {
  float: left;
  width: 20%;
  padding: 10px;
  background: #2196F3;
  color: white;
  font-size: 17px;
  border: 1px solid grey;
  border-left: none;
  cursor: pointer;
}

form.example button:hover{
  background: #0b7dda;
}

form.example::after {
  content: "";
  clear: both;
  display: table;
}
.pagination {
    padding-top: 5px;
  display: inline-block;
}

.pagination a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  transition: background-color .3s;
  border: 1px solid #ddd;
  margin: 0 4px;
}

.pagination a.active {
  background-color: #911EB3;
  color: white;
  border: 1px solid white;
}

.pagination a:hover:not(.active) {background-color: #ddd;}
thead{
    background-color: black;
    color: white;
    height: 24px;
    line-height: 24px;
}
table{
    width: 98%;
    margin-left: 1%;
    margin-right: 1%;
}
.tmbh{
    padding-top: 20px;
    padding-bottom: 5px;
    float: right;
    padding-right: 0.7%;
}
</style>
</head>

<body>
    <header>
        <div class="head">
            <div class="kiri-head">
                <img id="img-mobile" src="asset/images/wordmark4.png" alt="none">
            </div>
            <div class="kanan-head">
                <div class="button-div">
                    <img src="asset/images/avatar/ava2.jpg" alt="">
                    Wahyu
                </div>
            </div>
        </div>
    </header>
    <nav>
        <aside id="menu-mobile" class="menu-mobile">
            <ul>
                <li><a href="index.html">Dashboard</a></li>
                <li><a href="form.html">Transaksi</a></li>
                <li><a href="">Buku</a></li>
                <li><a href="">Pengarang</a></li>
                <li><a href="">Pengaturan</a></li>
            </ul>
        </aside>
        <aside id="menu" class="menu">
            <div class="head-img">
                <img id="head-img" src="asset/images/wordmark4.png" alt="none">
            </div>
            <ul>
                <li>
                    <a href="/BukuGlassFish/">
                        <img class="icon-menu" src="asset/images/icon/home.png" alt="none">
                        <span>Dashboard</span>
                    </a>
                </li
                <hr>
                <li>
                    <a href="/BukuGlassFish/transaksi?sort=buku">
                        <img class="icon-menu" src="asset/images/icon/apotik.png" alt="none">
                        <span>Transaksi</span>
                    </a>
                </li>
                <hr>
                <li>
                    <a href="/BukuGlassFish/buku">
                        <img class="icon-menu" src="asset/images/icon/kamar.png" alt="none">
                        <span>Buku</span>
                    </a>
                </li>
                <hr>
                <li>
                    <a href="/BukuGlassFish/pengarang">
                        <img class="icon-menu" src="asset/images/icon/petugas.png" alt="none">
                        <span>Pengarang</span>
                    </a>
                </li>
                <hr>
            </ul>
            <br>
            <br>
            <img id="button" class="panah" src="asset/images/icon/kiri.png" alt="none">
        </aside>
    </nav>
    <section>
        <article id="isi" class="isi">
            <div class="grid-container">
                <div class="baris">
                    <div class="kolom">
                        <div class="box">
                            <div class="box-container judul">
                                <h4><b>Daftar Buku</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-container">
                <div class="baris">
                    <div class="kolom">
                        <div class="box">
                            <!--<a class="btn btn-danger" href="#"></a>-->
                            <form class="example" method="get" action="/BukuGlassFish/buku">
                                <input type="hidden" name="page" value="search">
                                <input type="text" name="key" placeholder="Search.." name="search2">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>

                            <table>
                                <thead>
                                <tr>
                                    <th>NO BUKU</th>
                                    <th>NAMA BUKU</th>
                                    <th>TANGGAL</th>
                                    <th width="200px">ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <%
                                    List<BukuModel> buuk = (ArrayList) request.getAttribute("buku");
                                    for (BukuModel buku : buuk){%>
                                        <tr>
                                                <td><%=buku.getNoBuku()%></td>
                                                <td><%=buku.getNamaBuku()%></td>
                                                <td><%=buku.getTglTerbit()%></td>
                                                <td>
                                                    <a href="/BukuGlassFish/buku?page=details&nobuku=<%=buku.getNoBuku()%>"><button class="btnView"><i class="fa fa-eye"></i></button></a>
                                                    <a href="/BukuGlassFish/buku?page=edit&nobuku=<%=buku.getNoBuku()%>"><button class="btnEdit"><i class="fa fa-pencil"></i></button></a>
                                                    <button onclick='deleteItems("<%=buku.getNoBuku()%>","<%=buku.getNamaBuku()%>")'class="btnDelete"><i class="fa fa-trash"></i></button>
                                                </td>
                                        </tr>
                                        <%
                                    }
                                        %>
                                </tbody>
                            </table>
                            <div class="pagination">
                                <a href="#">&laquo;</a>
                                <a href="#" class="active">1</a>
                                <a href="#">2</a>
                                <a href="#">3</a>
                                <a href="#">4</a>
                                <a href="#">5</a>
                                <a href="#">6</a>
                                <a href="#">&raquo;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
    <footer>
        <div id="foot" class="foot">
            <div class="identitas">
                <strong>Wahyu Setiawan || 17.5.00173</strong>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="asset/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="asset/js/sidebar.js"></script>    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        function deleteItems(nobuku,namabuku)
        {
        swal({
            title: "Apa Kamu Yakin?",
            text: "Tindakan ini akan menghapus buku "+namabuku+" !!!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $.ajax({
                    type: 'POST',
                    url: 'http://localhost:8080/BukuGlassFish/buku?post=delete',
                    data: {
                        'nobuku': nobuku,
                        'namabuku' : namabuku
                    },
                    dataType : 'json',
                    success:function(data, status, xhttp){
                        console.log("got response as "+data);
                        if(data['kode'] == 200){
                        swal("Berhasi di Hapus !!!", "Buku telah berhasil di hapus !", "success").then(function(){
                            location.reload();
                        });
                        }
                    }
              });
            } else {
              swal("Membatalkan penghapusan buku!");
            }
          });
        }
    </script>
</body>

</html>
