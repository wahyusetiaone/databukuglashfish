package buku.DAO;

// kita perlu import Connection 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Koneksi {
    
    // Connection digunakan untuk menyimpan setting koneksi database
    private static Connection conn;
    
    // definisikan lokasi driver database
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    
    // definisikan url dari server database (localhost)
    private static final String URL = "jdbc:mysql://localhost:3306/data_buku";
    
    // definisikan username dari database
    private static final String USERNAME = "root";
    
    // definisikan password dari database
    private static final String PASSWORD = "1";
    
    // definisikan method untuk memberikan nilai return conn
    // conn adalah hasil setting koneksi database aplikasi kita
    public static Connection getKoneksi() {
        
        // cek variabel conn apakah kosong
        if (conn == null) {
            
            // dalam java kita sering memakai try-catch 
            // untuk menangkap eksepsi (perkiraan error yg akan
            // terjadi)
            try {
                
                // panggil lokasi driver kita
                Class.forName(DRIVER);
                System.out.println("Driver ditemukan.");
                
                // setelah kita cek lokasi driver, kita setting conn
                try {
                    
                    conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                    System.out.println("Koneksi basis data berhasil.");
                    
                } catch(SQLException se) {
                    
                    System.out.println("Koneksi basis data gagal: " + se);
                    
                }
            
            } catch(ClassNotFoundException cnfe) {
                
                System.out.println("Driver tidak ditemukan: " + cnfe);
                
            }
            
        }
        
        return conn;
        
    }
    
    
}
