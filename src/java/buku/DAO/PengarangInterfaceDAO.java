/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.DAO;

import buku.Model.PengarangModel;
import java.util.List;

/**
 *
 * @author abah
 */
public interface PengarangInterfaceDAO {
   public void addPengarang(PengarangModel pengarang);
   public List<PengarangModel> getAllPengarang();
   public PengarangModel getPengarang(int nopengarang);
   public void updatePengarang(PengarangModel pengarang);
   public void deletePengarang(int nopengarang, String namapengarang);   
   public List<PengarangModel> searchPengarang(String search);
}
