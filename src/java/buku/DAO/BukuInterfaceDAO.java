/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.DAO;
import buku.Model.BukuModel;
import java.util.List;

/**
 *
 * @author abah
 */
public interface BukuInterfaceDAO <BukuModel>{
   public void addBuku(BukuModel buku);
   public List<BukuModel> getAllBuku();
   public BukuModel getBuku(int nobuku);
   public void updateBuku(BukuModel buku);
   public void deleteBuku(int nobuku, String namabuku);   
   public List<BukuModel> searchBuku(String search);

}
