/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.DAO;

import buku.Model.TransaksiModel;
import java.util.List;

/**
 *
 * @author abah
 */
public interface TransaksiInterfaceDAO{
   public void addTransaksi(int no, int noo, String namapenerbit, String alamatpenerbit);
   public List<TransaksiModel> getAllTransaksi();
   public List<TransaksiModel> getTransaksi(String mode,int notransaksi,int nobuku,int nopengarang);
   public void updateTransaksi(int no,int noo,String nama_penerbit,String alamat_penerbit,String tglterbit,String mode);
   public void deleteTransaksi(int no, String mode);   
   public List<TransaksiModel> searchTransaksi(String search);

   public void tambahItems(int no,int noo,String nama_penerbit,String alamat_penerbit,String mode);
   public void updateItems(int notransaksi,int no,int nobaru,String mode);
   public void deleteItems(int notransaksi,int no,int noo,String mode);
}
