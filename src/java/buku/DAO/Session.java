/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.DAO;

/**
 *
 * @author Isnan Alcantara
 */
public class Session {
    
    private static int id_session;
    private static String password_session;
    private static String nama_session;
    private static String jabatan_session;
    public static void setIdSession(int id_login) {
        
        id_session = id_login;
        
    }
    
    public static int getIdSession() {
        
        return id_session;
        
    }
    
    public static void setPasswordSession(String password_login) {
        
        password_session = password_login;
        
    }
    
    public static String getPasswordSession() {
        
        return password_session;
        
    }
    
    public static void setNamaSession(String nama_login) {
        
        nama_session = nama_login;
        
    }
    
    public static String getNamaSession() {
        
        return nama_session;
        
    }
    
    public static void setJabatanSession(String jabatan_login) {
        
        jabatan_session = jabatan_login;
        
    }
    
    public static String getJabatanSession() {
        
        return jabatan_session;
        
    }
}
