/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.DAO;

import buku.Model.BukuModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author abah
 */
public class DatabasesAccessObject {
    // Connection digunakan untuk menyimpan setting koneksi database
    private static Connection connection;
    
    // definisikan lokasi driver database
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    
    // definisikan url dari server database (localhost)
    private static final String URL = "jdbc:mysql://localhost:3306/data_buku";
    
    // definisikan username dari database
    private static final String USERNAME = "root";
    
    // definisikan password dari database
    private static final String PASSWORD = "1";
    
    public DatabasesAccessObject() throws SQLException{
        if (connection == null) {
            
            // dalam java kita sering memakai try-catch 
            // untuk menangkap eksepsi (perkiraan error yg akan
            // terjadi)
            try {
                
                // panggil lokasi driver kita
                Class.forName(DRIVER);
                System.out.println("Driver ditemukan.");
                
                // setelah kita cek lokasi driver, kita setting conn
                try {
                    
                    connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                    System.out.println("Koneksi basis data berhasil.");
                    
                } catch(SQLException se) {
                    
                    System.out.println("Koneksi basis data gagal: " + se);
                    
                }
            
            } catch(ClassNotFoundException cnfe) {
                
                System.out.println("Driver tidak ditemukan: " + cnfe);
                
            }
            
        }
        
    }
    
//    public void tambahMahasiswa(Mahasiswa mahasiswa) throws SQLException{
//        PreparedStatement ps=connection.prepareStatement("INSERT INTO mahasiswa values(?,?,?,?,?)");
//        ps.setString(1, mahasiswa.getNim());
//        ps.setString(2, mahasiswa.getNama());
//        ps.setInt(3, mahasiswa.getUmur());
//        ps.setString(4, mahasiswa.getAsalSekolah());
//        ps.setString(5, mahasiswa.getAlamat());
//        ps.execute();
//    }
    
    public List<BukuModel> getAllBuku() throws SQLException{
        List<BukuModel> storageBuku=new ArrayList<BukuModel>();
        PreparedStatement ps=connection.prepareStatement("SELECT * FROM t_buku");
        ResultSet rs=ps.executeQuery();
        System.out.println("Accesss");
        while(rs.next()){
            BukuModel mbuku=new BukuModel();
            mbuku.setNoBuku(rs.getInt("no_buku"));
            mbuku.setNamaBuku(rs.getString("nama_buku")); 
            mbuku.setTglTerbit(rs.getString("tgl_terbit"));
            storageBuku.add(mbuku);
        }
        return storageBuku;
        
    }
    
//    public Mahasiswa getMahasiswa(String nim) throws SQLException{
//        PreparedStatement ps=connection.prepareStatement("SELECT * FROM mahasiswa where nim=?");
//        ps.setString(1, nim);
//        ResultSet rs=ps.executeQuery();
//        Mahasiswa mahasiswa=new Mahasiswa();
//        while(rs.next()){
//            mahasiswa.setNim(rs.getString("nim"));
//            mahasiswa.setNama(rs.getString("nama"));
//            mahasiswa.setUmur(rs.getInt("umur"));
//            mahasiswa.setAsalSekolah(rs.getString("asal_sekolah"));
//            mahasiswa.setAlamat(rs.getString("alamat"));
//        }
//            return mahasiswa;
//    }
//    
//    public void updateMahasiswa(Mahasiswa mahasiswa) throws SQLException{
//        PreparedStatement ps=connection.prepareStatement("UPDATE mahasiswa set nama=?,umur=?,asal_sekolah=?,"
//                + "alamat=? where nim=?");
//        ps.setString(1, mahasiswa.getNama());
//        ps.setInt(2, mahasiswa.getUmur());
//        ps.setString(3, mahasiswa.getAsalSekolah());
//        ps.setString(4, mahasiswa.getAlamat());
//        ps.setString(1, mahasiswa.getNim());
//        ps.executeUpdate();
//    }
//    
//    public void hapusMahasiswa(String nim) throws SQLException{
//        PreparedStatement ps=connection.prepareStatement("DELETE FROM mahasiswa where nim=?");
//        ps.setString(1, nim);
//        ps.executeUpdate();
//    }
}
