/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.Controller;

import buku.Implementasi.PengarangImplDAO;
import buku.Model.PengarangModel;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author abah
 */
public class PengarangController extends HttpServlet{
    PengarangImplDAO Pengarang;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String page=request.getParameter("page");
        String view=null;
        Pengarang=new PengarangImplDAO();
        if(page!=null){
            if(page.equals("tambah")){
                
                view="tambahpengarang";
                
            }else if(page.equals("details")){
                
                view="detailspengarang";
                    String nopengarang=request.getParameter("nopengarang");
                    PengarangModel data=Pengarang.getPengarang(Integer.parseInt(nopengarang));
                    request.setAttribute("data", data);
            }else if(page.equals("edit")){
                
                view="editpengarang";
                    String nopengarang=request.getParameter("nopengarang");
                    PengarangModel data=Pengarang.getPengarang(Integer.parseInt(nopengarang));
                    request.setAttribute("data", data);

            }else if(page.equals("search")){
                
                view="pengarang";
                String key=request.getParameter("key");
                List<PengarangModel> pengarang=Pengarang.searchPengarang(key);
                request.setAttribute("pengarang", pengarang);

            }
        }else{
            view="pengarang";
            List<PengarangModel> pengarang=Pengarang.getAllPengarang();
            request.setAttribute("pengarang", pengarang);
        }
        String url="/"+view+".jsp";
        request.getRequestDispatcher(url).forward(request, response);
    }
 
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Pengarang=new PengarangImplDAO();
        String post=request.getParameter("post");
        
        if(post.equals("tambah")){
            System.out.println(request.getParameter("alamatpengarang"));
            PengarangModel pengarang = new PengarangModel();
            pengarang.setNamaPengarang(request.getParameter("namapengarang"));
            pengarang.setAlamatPengarang(request.getParameter("alamatpengarang"));
            Pengarang.addPengarang(pengarang);
            
            JSONObject obj = new JSONObject();

            obj.put("kode",200);
            obj.put("string","success");

            response.setContentType("application/json; charset=utf-8");
            response.getWriter().write(obj.toJSONString());
        }else if(post.equals("edit")){
            String nopengarang = request.getParameter("nopengarang");
            PengarangModel pengarang = new PengarangModel();
            pengarang.setNoPengarang(Integer.parseInt(request.getParameter("nopengarang")));
            pengarang.setNamaPengarang(request.getParameter("namapengarang"));
            pengarang.setAlamatPengarang(request.getParameter("alamatpengarang"));
            Pengarang.updatePengarang(pengarang);
            response.sendRedirect("/BukuGlassFish/pengarang?page=details&nopengarang="+nopengarang);
        }else if(post.equals("delete")){
            String nopengarang = request.getParameter("nopengarang");
            String namapengarang = request.getParameter("namapengarang");
            Pengarang.deletePengarang(Integer.parseInt(nopengarang), namapengarang);
            
            JSONObject obj = new JSONObject();

            obj.put("kode",200);
            obj.put("string","success");

            response.setContentType("application/json; charset=utf-8");
            response.getWriter().write(obj.toJSONString());
        }
    }
}
