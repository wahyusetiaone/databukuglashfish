/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.Controller;

import buku.Implementasi.BukuImplDAO;
import buku.Implementasi.PengarangImplDAO;
import buku.Implementasi.TransaksiImplDAO;
import buku.Model.BukuModel;
import buku.Model.PengarangModel;
import buku.Model.TransaksiModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author abah
 */
public class TransaksiController extends HttpServlet{
    TransaksiImplDAO Transaksi;
    BukuImplDAO Buku;
    PengarangImplDAO Pengarang;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String page=request.getParameter("page");
        String view=null;
        Transaksi=new TransaksiImplDAO();
        Buku=new BukuImplDAO();
        Pengarang=new PengarangImplDAO();
        if(page!=null){
            if(page.equals("tambah.jsp")){
                
                view="tambahpengarang";
                
            }else if(page.equals("search")){
                view="transaksi.jsp";
                String sort=request.getParameter("sort");
                String key=request.getParameter("key");
                List<TransaksiModel> trans=Transaksi.searchTransaksi(key);
                Map<Integer, TransaksiModel> map = new HashMap<>();
                if(sort.equals("buku")){System.out.println(sort);

                    for(TransaksiModel tr : trans){
                        if(!map.containsKey(tr.getNoBuku())){
                            map.put(tr.getNoBuku(), tr);
                        }else{
                            String old = map.get(tr.getNoBuku()).getNamaPengarang();
                            String baru = tr.getNamaPengarang();
                            TransaksiModel mtr = tr;
                            mtr.setNamaPengarang(baru +", "+ old);
                            map.put(tr.getNoBuku(), mtr);
                        }
                    }
                }else if(sort.equals("pengarang")){
                    for(TransaksiModel tr : trans){
                        if(!map.containsKey(tr.getNoPengarang())){
                            map.put(tr.getNoPengarang(), tr);
                        }else{
                            String old = map.get(tr.getNoPengarang()).getNamaBuku();
                            String baru = tr.getNamaBuku();
                            TransaksiModel mtr = tr;
                            mtr.setNamaBuku(baru +", "+ old);
                            map.put(tr.getNoPengarang(), mtr);
                        }
                    }
                }
                request.setAttribute("data", map);
                }else if(page.equals("details")){
                    view="detailstransaksi.jsp";
                        String mode=request.getParameter("mode");
                        String notransaksi=request.getParameter("notransaksi");
                        String nobuku=request.getParameter("nobuku");
                        String nopengarang=request.getParameter("nopengarang");
                        List<TransaksiModel> data=Transaksi.getTransaksi(mode,Integer.parseInt(notransaksi),Integer.parseInt(nobuku),Integer.parseInt(nopengarang));
                        List<BukuModel> dbuku=Buku.getAllBuku();
                        List<PengarangModel> dpengarang=Pengarang.getAllPengarang();
                        request.setAttribute("data", data);
                        request.setAttribute("dbuku", dbuku);
                        request.setAttribute("dpengarang", dpengarang);
                }
//                else if(page.equals("edit")){
//                
//                view="editpengarang";
//                    String nopengarang=request.getParameter("nopengarang");
//                    PengarangModel data=Pengarang.getPengarang(Integer.parseInt(nopengarang));
//                    request.setAttribute("data", data);
//
//            }else if(page.equals("search")){
//                
//                view="pengarang";
//                String key=request.getParameter("key");
//                List<PengarangModel> pengarang=Pengarang.searchPengarang(key);
//                request.setAttribute("pengarang", pengarang);
//
//            }
        }else{
            view="transaksi.jsp";
            String sort=request.getParameter("sort");
            List<TransaksiModel> trans=Transaksi.getAllTransaksi();
            Map<Integer, TransaksiModel> map = new HashMap<>();
            if(sort.equals("buku")){System.out.println(sort);

                for(TransaksiModel tr : trans){
                    if(!map.containsKey(tr.getNoBuku())){
                        map.put(tr.getNoBuku(), tr);
                    }else{
                        String old = map.get(tr.getNoBuku()).getNamaPengarang();
                        String baru = tr.getNamaPengarang();
                        TransaksiModel mtr = tr;
                        mtr.setNamaPengarang(baru +", "+ old);
                        map.put(tr.getNoBuku(), mtr);
                    }
                }
            }else if(sort.equals("pengarang")){
                for(TransaksiModel tr : trans){
                    if(!map.containsKey(tr.getNoPengarang())){
                        map.put(tr.getNoPengarang(), tr);
                    }else{
                        String old = map.get(tr.getNoPengarang()).getNamaBuku();
                        String baru = tr.getNamaBuku();
                        TransaksiModel mtr = tr;
                        mtr.setNamaBuku(baru +", "+ old);
                        map.put(tr.getNoPengarang(), mtr);
                    }
                }
            }
            
//            for (Map.Entry<Integer, TransaksiModel> entry : map.entrySet()) {
//                System.out.println(entry.getKey() + "/" + entry.getValue().getNamaPengarang());
//            }
            request.setAttribute("data", map);
        }
        String url="/"+view;
        request.getRequestDispatcher(url).forward(request, response);
    }
 
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            Transaksi=new TransaksiImplDAO();
            String post=request.getParameter("post");
            
            if(post.equals("tambah")){
                String mode = request.getParameter("mode");
                String no = request.getParameter("no");
                String namapenerbit = request.getParameter("namapenerbit");
                String alamatpenerbit = request.getParameter("alamatpenerbit");
                boolean loop = true;
                int a =0;
                while(loop){
                    String noo = request.getParameter("noo["+a+"]");
                    System.out.println(no);

                    if(noo!=null && !noo.isEmpty()){
                        if(mode.equals("buku")){
                            Transaksi.addTransaksi(Integer.parseInt(no), Integer.parseInt(noo), namapenerbit, alamatpenerbit);

                        }else if(mode.equals("pengarang")){
                            Transaksi.addTransaksi(Integer.parseInt(noo), Integer.parseInt(no), namapenerbit, alamatpenerbit);
                        }
                        a++;
                    }else{
                        loop = false;
                        }
                    }
                            response.sendRedirect("/BukuGlassFish/transaksi?sort=buku");
            }else if(post.equals("edititem")){
                String mode=request.getParameter("mode");

                if(mode.equals("buku")){
                String notr = request.getParameter("notr");
                String no = request.getParameter("no");
                String nobaru = request.getParameter("nobaru");                    
                System.out.println(request.getParameter("nobaru"));

                    Transaksi.updateItems(Integer.parseInt(notr), Integer.parseInt(no), Integer.parseInt(nobaru), mode);
                    response.sendRedirect("/BukuGlassFish/transaksi?page=details&mode=buku&notransaksi="+notr+"&nobuku="+no+"&nopengarang="+nobaru);

                }else if(mode.equals("pengarang")){
                String notr = request.getParameter("notr");
                String no = request.getParameter("no");
                String nobaru = request.getParameter("nobaru");                    
                System.out.println(request.getParameter("nobaru"));

                    Transaksi.updateItems(Integer.parseInt(notr), Integer.parseInt(no), Integer.parseInt(nobaru), mode);
                    response.sendRedirect("/BukuGlassFish/transaksi?page=details&mode=pengarang&notransaksi="+notr+"&nobuku="+nobaru+"&nopengarang="+no);
 
                }
            }else if(post.equals("deleteitem")){
                String mode = request.getParameter("mode");
                String notr = request.getParameter("notr");
                String no = request.getParameter("no");
                String noo = request.getParameter("noo");
                if(mode.equals("buku") || mode.equals("pengarang")){

                    Transaksi.deleteItems(Integer.parseInt(notr), Integer.parseInt(no), Integer.parseInt(noo), mode);

                    JSONObject obj = new JSONObject();

                    obj.put("kode",200);
                    obj.put("string","success");

                    response.setContentType("application/json; charset=utf-8");
                    response.getWriter().write(obj.toJSONString());
                }
            }else if(post.equals("tambahitem")){
                String no = request.getParameter("no");
                String noo = request.getParameter("noo");
                String mode = request.getParameter("mode");
                String namapenerbit = request.getParameter("alamatpenerbit");
                String alamatpenerbit = request.getParameter("alamatpenerbit");
                Transaksi.tambahItems(Integer.parseInt(no), Integer.parseInt(noo), namapenerbit, alamatpenerbit,mode);
                if(mode.equals("buku")){
                response.sendRedirect("/BukuGlassFish/transaksi?page=details&mode="+mode+"&notransaksi=0&nobuku="+no+"&nopengarang=0");
                }else if(mode.equals("pengarang")){
                response.sendRedirect("/BukuGlassFish/transaksi?page=details&mode="+mode+"&notransaksi=0&nobuku=0&nopengarang="+no);

                }
            }else if(post.equals("deletemain")){
                String no = request.getParameter("no");
                String mode = request.getParameter("mode");

                    Transaksi.deleteTransaksi(Integer.parseInt(no),mode);

                    JSONObject obj = new JSONObject();

                    obj.put("kode",200);
                    obj.put("redirect","http://localhost:8080/BukuGlassFish/transaksi?sort="+mode);
                    obj.put("string","success");

                    response.setContentType("application/json; charset=utf-8");
                    response.getWriter().write(obj.toJSONString());
            }else if(post.equals("editmain")){
                String mode = request.getParameter("mode");
                String no = request.getParameter("no");
                System.out.print(no);
                String namapenerbit = request.getParameter("namapenerbit");
                String alamatpenerbit = request.getParameter("alamatpenerbit");
                String tglterbit = request.getParameter("tglterbit");
                boolean loop = true;
                int a =0;
                while(loop){
                    String noo = request.getParameter("noo["+a+"]");
                    if(noo!=null && !noo.isEmpty()){
                        Transaksi.updateTransaksi(Integer.parseInt(no), Integer.parseInt(noo), namapenerbit, alamatpenerbit, tglterbit, mode);
                        a++;
                    }else{
                        loop = false;
                        if(mode.equals("buku")){
                        response.sendRedirect("/BukuGlassFish/transaksi?page=details&mode=buku&notransaksi="+0+"&nobuku="+no+"&nopengarang="+0);
                        }else if(mode.equals("pengarang")){
                        response.sendRedirect("/BukuGlassFish/transaksi?page=details&mode=pengarang&notransaksi="+0+"&nobuku="+0+"&nopengarang="+no);
                        }
                    }
            }
        }
    }
}
