/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.Controller;

import buku.Implementasi.BukuImplDAO;
import buku.Model.BukuModel;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author abah
 */
public class BukuController extends HttpServlet{
    BukuImplDAO Buku;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String page=request.getParameter("page");
        String view=null;
        Buku=new BukuImplDAO();
        if(page!=null){
            if(page.equals("tambah")){
                
                view="tambahbuku";
                
            }else if(page.equals("details")){
                
                view="detailsbuku";
                    String nobuku=request.getParameter("nobuku");
                    BukuModel data=Buku.getBuku(Integer.parseInt(nobuku));
                    request.setAttribute("data", data);
            }else if(page.equals("edit")){
                
                view="editbuku";
                    String nobuku=request.getParameter("nobuku");
                    BukuModel data=Buku.getBuku(Integer.parseInt(nobuku));
                    request.setAttribute("data", data);

            }else if(page.equals("search")){
                
                view="buku";
                String key=request.getParameter("key");
                List<BukuModel> buku=Buku.searchBuku(key);
                request.setAttribute("buku", buku);

            }
        }else{
            view="buku";
            List<BukuModel> buku=Buku.getAllBuku();
            request.setAttribute("buku", buku);
        }
        String url="/"+view+".jsp";
        request.getRequestDispatcher(url).forward(request, response);
    }
 
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Buku=new BukuImplDAO();
        String post=request.getParameter("post");
        
        if(post.equals("tambah")){
            System.out.println(request.getParameter("namabuku"));
            BukuModel buku = new BukuModel();
            buku.setNamaBuku(request.getParameter("namabuku"));
            buku.setIsbnBuku(request.getParameter("isbnbuku"));
            buku.setTglTerbit(request.getParameter("tglterbit"));
            Buku.addBuku(buku);
            JSONObject obj = new JSONObject();

            obj.put("kode",200);
            obj.put("string","success");

            response.setContentType("application/json; charset=utf-8");
            response.getWriter().write(obj.toJSONString());
        }else if(post.equals("edit")){
            String nobuku = request.getParameter("nobuku");
            BukuModel buku = new BukuModel();
            buku.setNoBuku(Integer.parseInt(request.getParameter("nobuku")));
            buku.setNamaBuku(request.getParameter("namabuku"));
            buku.setIsbnBuku(request.getParameter("isbnbuku"));
            buku.setTglTerbit(request.getParameter("tglterbit"));
            Buku.updateBuku(buku);
            response.sendRedirect("/BukuGlassFish/buku?page=details&nobuku="+nobuku);
        }else if(post.equals("delete")){
            String nobuku = request.getParameter("nobuku");
            String namabuku = request.getParameter("namabuku");
            Buku.deleteBuku(Integer.parseInt(nobuku), namabuku);
            
            JSONObject obj = new JSONObject();

            obj.put("kode",200);
            obj.put("string","success");

            response.setContentType("application/json; charset=utf-8");
            response.getWriter().write(obj.toJSONString());
        }
    }
}
