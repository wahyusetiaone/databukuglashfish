/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.Implementasi;

import buku.DAO.BukuInterfaceDAO;
import buku.DAO.Koneksi;
import buku.Model.BukuModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abah
 */
public class BukuImplDAO implements BukuInterfaceDAO<BukuModel> {
    private List<BukuModel> storageBuku;
    Koneksi connection = new Koneksi();
    
    @Override
   public void addBuku(BukuModel buku) {
       System.out.println(buku.getNamaBuku());
       try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("INSERT INTO t_buku (no_buku, nama_buku, isbn, tgl_terbit) VALUES ("+this.lastRecordNoBuku()+",'"+buku.getNamaBuku()+"','"+buku.getIsbnBuku()+"','"+buku.getTglTerbit()+"')");
            ps.execute();
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
      System.out.println("BukuModel: Roll No " + buku.getNoBuku() + ", deleted from database");
   }

   @Override
   public void deleteBuku(int nobuku, String namabuku) {
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("DELETE FROM t_buku WHERE no_buku = "+nobuku);
            ps.executeUpdate();
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
        }
   }

   //retrive list of buku from the database
   @Override
   public List<BukuModel> getAllBuku() {
//      return buku;
        storageBuku= new ArrayList<BukuModel>();
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("SELECT * FROM t_buku");
            ResultSet rs=ps.executeQuery();
                while(rs.next()){
                    BukuModel mbuku=new BukuModel();
                    mbuku.setNoBuku(rs.getInt("no_buku"));
                    mbuku.setNamaBuku(rs.getString("nama_buku")); 
                    mbuku.setTglTerbit(rs.getString("tgl_terbit"));
                    storageBuku.add(mbuku);
                    
                } 
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
     
        return storageBuku;
   }

   @Override
   public BukuModel getBuku(int nobuku) {        
       BukuModel buku=new BukuModel();
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("SELECT * FROM t_buku WHERE no_buku = "+nobuku);
            ResultSet rs=ps.executeQuery();
                while(rs.next()){
                    buku.setNoBuku(rs.getInt("no_buku"));
                    buku.setNamaBuku(rs.getString("nama_buku")); 
                    buku.setIsbnBuku(rs.getString("isbn")); 
                    buku.setTglTerbit(rs.getString("tgl_terbit"));                    
                } 
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
       return buku;
   }

   @Override
   public void updateBuku(BukuModel buku) {
       try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("UPDATE t_buku SET nama_buku = '"+buku.getNamaBuku()+"', isbn = '"+buku.getIsbnBuku()+"', tgl_terbit = '"+buku.getTglTerbit()+"' WHERE no_buku = "+buku.getNoBuku());
            ps.executeUpdate();
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
      System.out.println("BukuModel: Roll No " + buku.getNamaBuku() + ", updated in the database");
   }
   
   private int lastRecordNoBuku(){
       int nobuku = 0;
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("select no_buku as no from t_buku ORDER BY no_buku DESC LIMIT 1;");
            ResultSet rs=ps.executeQuery();
            rs.absolute(1);
            nobuku = rs.getInt("no");
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
       
       return (nobuku+1);
   }
   @Override
   public List<BukuModel> searchBuku(String search) {
//      return buku;
        storageBuku= new ArrayList<BukuModel>();
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("SELECT * FROM t_buku WHERE no_buku LIKE '%"+search+"%' || nama_buku LIKE '%"+search+"%' || isbn LIKE '%"+search+"%'");
            ResultSet rs=ps.executeQuery();
                while(rs.next()){
                    BukuModel mbuku=new BukuModel();
                    mbuku.setNoBuku(rs.getInt("no_buku"));
                    mbuku.setNamaBuku(rs.getString("nama_buku")); 
                    mbuku.setTglTerbit(rs.getString("tgl_terbit"));
                    storageBuku.add(mbuku);
                    
                } 
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
     
        return storageBuku;
   }
}

