/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.Implementasi;

import buku.DAO.Koneksi;
import buku.DAO.PengarangInterfaceDAO;
import buku.Model.PengarangModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abah
 */
public class PengarangImplDAO implements PengarangInterfaceDAO{
    private List<PengarangModel> storagePengarang;
    Koneksi connection = new Koneksi();
    
    @Override
    public void addPengarang(PengarangModel pengarang) {
         try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("INSERT INTO t_pengarang (id_pengarang, nama_pengarang, alamat_pengarang) VALUES ("+this.lastRecordNoPengarang()+",'"+pengarang.getNamaPengarang()+"','"+pengarang.getAlamatPengarang()+"')");
            ps.execute();
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
        }
    }

    @Override
    public List<PengarangModel> getAllPengarang() {
        storagePengarang= new ArrayList<PengarangModel>();
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("SELECT * FROM t_pengarang");
            ResultSet rs=ps.executeQuery();
            System.out.println("Accesss");
                while(rs.next()){
                    PengarangModel mpengarang=new PengarangModel();
                    mpengarang.setNoPengarang(rs.getInt("id_pengarang"));
                    mpengarang.setNamaPengarang(rs.getString("nama_pengarang")); 
                    mpengarang.setAlamatPengarang(rs.getString("alamat_pengarang"));
                    storagePengarang.add(mpengarang);
                    
                } 
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
     
        return storagePengarang;
    }

    @Override
    public PengarangModel getPengarang(int nopengarang) {
        PengarangModel pengarang=new PengarangModel();
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("SELECT * FROM t_pengarang WHERE id_pengarang = "+nopengarang);
            ResultSet rs=ps.executeQuery();
                while(rs.next()){
                    pengarang.setNoPengarang(rs.getInt("id_pengarang"));
                    pengarang.setNamaPengarang(rs.getString("nama_pengarang")); 
                    pengarang.setAlamatPengarang(rs.getString("alamat_pengarang"));                    
                } 
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
       return pengarang;
    }

    @Override
    public void updatePengarang(PengarangModel pengarang) {
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("UPDATE t_pengarang SET nama_pengarang = '"+pengarang.getNamaPengarang()+"', alamat_pengarang = '"+pengarang.getAlamatPengarang()+"' WHERE id_pengarang = "+pengarang.getNoPengarang());
            ps.executeUpdate();
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
    }

    @Override
    public void deletePengarang(int nopengarang, String namapengarang) {
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("DELETE FROM t_pengarang WHERE id_pengarang = "+nopengarang);
            ps.executeUpdate();
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
        }
    }
    
    public List<PengarangModel> searchPengarang(String search) {
        storagePengarang= new ArrayList<PengarangModel>();
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("SELECT * FROM t_pengarang WHERE id_pengarang LIKE '%"+search+"%' || nama_pengarang LIKE '%"+search+"%' || alamat_pengarang LIKE '%"+search+"%'");
            ResultSet rs=ps.executeQuery();
            System.out.println("Accesss");
                while(rs.next()){
                    PengarangModel mpengarang=new PengarangModel();
                    mpengarang.setNoPengarang(rs.getInt("id_pengarang"));
                    mpengarang.setNamaPengarang(rs.getString("nama_pengarang")); 
                    mpengarang.setAlamatPengarang(rs.getString("alamat_pengarang"));
                    storagePengarang.add(mpengarang);
                    
                } 
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
     
        return storagePengarang;
    }
    
       private int lastRecordNoPengarang(){
       int nopengarang = 0;
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("select id_pengarang as no from t_pengarang ORDER BY id_pengarang DESC LIMIT 1;");
            ResultSet rs=ps.executeQuery();
            rs.absolute(1);
            nopengarang = rs.getInt("no");
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
       
       return (nopengarang+1);
   }
    
}
