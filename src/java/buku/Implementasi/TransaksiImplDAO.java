/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.Implementasi;

import buku.DAO.Koneksi;
import buku.DAO.TransaksiInterfaceDAO;
import buku.Model.TransaksiModel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author abah
 */
public class TransaksiImplDAO implements TransaksiInterfaceDAO{
    private List<TransaksiModel> storageTransaksi;
    Koneksi connection = new Koneksi();
    @Override
    public void addTransaksi(int no, int noo, String namapenerbit, String alamatpenerbit) {
        
       try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("INSERT INTO t_databuku (id_databuku, no_buku, id_pengarang, nama_penerbit, alamat_penerbit, id) VALUES ("+this.lastRecordNoTransaksi()+","+no+","+noo+",'"+namapenerbit+"','"+alamatpenerbit+"',"+this.lastRecordNoTransaksi()+")");
            ps.execute();
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
    }

    @Override
    public List<TransaksiModel> getAllTransaksi() {
                System.out.println("Accesss");
        storageTransaksi= new ArrayList<TransaksiModel>();       
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("select b.no_buku,b.nama_buku,b.isbn,b.tgl_terbit,p.id_pengarang,p.nama_pengarang,p.alamat_pengarang,d.nama_penerbit,d.alamat_penerbit,d.id_databuku,d.id from t_buku b inner join (t_databuku d inner join t_pengarang p on d.id_pengarang = p.id_pengarang) ON b.no_buku = d.no_buku");
            ResultSet rs=ps.executeQuery();
                while(rs.next()){
                    TransaksiModel mt=new TransaksiModel();
                    mt.setNoTransaksi(rs.getInt("id_databuku"));
                    mt.setNamaPenerbit(rs.getString("nama_penerbit")); 
                    mt.setAlamatPenerbit(rs.getString("alamat_penerbit"));
                    mt.setNoBuku(rs.getInt("no_buku"));
                    mt.setIsbnBuku(rs.getString("isbn"));
                    mt.setNamaBuku(rs.getString("nama_buku")); 
                    mt.setTglTerbit(rs.getString("tgl_terbit"));
                    mt.setNoPengarang(rs.getInt("id_pengarang"));
                    mt.setNamaPengarang(rs.getString("nama_pengarang")); 
                    mt.setAlamatPengarang(rs.getString("alamat_pengarang"));
                    mt.setNoUnix(rs.getInt("id"));
                    storageTransaksi.add(mt);
                    
                } 
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }

        return storageTransaksi;
    }

    @Override
    public List<TransaksiModel> getTransaksi(String mode,int notransaksi,int nobuku,int nopengarang) {
        storageTransaksi= new ArrayList<TransaksiModel>();

        if(mode.equals("buku")){
            try {
                PreparedStatement ps=connection.getKoneksi().prepareStatement("select b.no_buku,b.nama_buku,b.isbn,b.tgl_terbit,p.id_pengarang,p.nama_pengarang,p.alamat_pengarang,d.nama_penerbit,d.alamat_penerbit,d.id_databuku,d.id from t_buku b inner join (t_databuku d inner join t_pengarang p on d.id_pengarang = p.id_pengarang) ON b.no_buku = d.no_buku WHERE b.no_buku ="+nobuku);
                ResultSet rs=ps.executeQuery();
                System.out.println("Accesss");
                    while(rs.next()){
                        TransaksiModel mt=new TransaksiModel();
                        mt.setNoTransaksi(rs.getInt("id_databuku"));
                        mt.setNamaPenerbit(rs.getString("nama_penerbit")); 
                        mt.setAlamatPenerbit(rs.getString("alamat_penerbit"));
                        mt.setNoBuku(rs.getInt("no_buku"));
                        mt.setIsbnBuku(rs.getString("isbn"));
                        mt.setNamaBuku(rs.getString("nama_buku")); 
                        mt.setTglTerbit(rs.getString("tgl_terbit"));
                        mt.setNoPengarang(rs.getInt("id_pengarang"));
                        mt.setNamaPengarang(rs.getString("nama_pengarang")); 
                        mt.setAlamatPengarang(rs.getString("alamat_pengarang"));
                        mt.setNoUnix(rs.getInt("id"));

                        storageTransaksi.add(mt);

                    } 
                } catch(SQLException se) {

                System.out.println("data gagal: " + se);
                }
        }else if(mode.equals("pengarang")){
            try {
                PreparedStatement ps=connection.getKoneksi().prepareStatement("select b.no_buku,b.nama_buku,b.isbn,b.tgl_terbit,p.id_pengarang,p.nama_pengarang,p.alamat_pengarang,d.nama_penerbit,d.alamat_penerbit,d.id_databuku,d.id from t_buku b inner join (t_databuku d inner join t_pengarang p on d.id_pengarang = p.id_pengarang) ON b.no_buku = d.no_buku WHERE p.id_pengarang ="+nopengarang);
                ResultSet rs=ps.executeQuery();
                System.out.println("Accesss");
                    while(rs.next()){
                        TransaksiModel mt=new TransaksiModel();
                        mt.setNoTransaksi(rs.getInt("id_databuku"));
                        mt.setNamaPenerbit(rs.getString("nama_penerbit")); 
                        mt.setAlamatPenerbit(rs.getString("alamat_penerbit"));
                        mt.setNoBuku(rs.getInt("no_buku"));
                        mt.setIsbnBuku(rs.getString("isbn"));
                        mt.setNamaBuku(rs.getString("nama_buku")); 
                        mt.setTglTerbit(rs.getString("tgl_terbit"));
                        mt.setNoPengarang(rs.getInt("id_pengarang"));
                        mt.setNamaPengarang(rs.getString("nama_pengarang")); 
                        mt.setAlamatPengarang(rs.getString("alamat_pengarang"));
                        mt.setNoUnix(rs.getInt("id"));

                        storageTransaksi.add(mt);

                    } 
                } catch(SQLException se) {

                System.out.println("data gagal: " + se);
                }
        }
        return storageTransaksi;
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateTransaksi(int no,int noo,String nama_penerbit,String alamat_penerbit,String tglterbit,String mode) {
        if(mode.equals("buku")){
            try {
                PreparedStatement ps=connection.getKoneksi().prepareStatement("UPDATE t_databuku SET no_buku = '"+no+"', nama_penerbit = '"+nama_penerbit+"', alamat_penerbit = '"+alamat_penerbit+"' WHERE id_databuku = "+noo);
                ps.executeUpdate();
                PreparedStatement ps2=connection.getKoneksi().prepareStatement("UPDATE t_buku SET tgl_terbit = '"+tglterbit+"' WHERE no_buku = "+no);
                ps2.executeUpdate();
                } catch(SQLException se) {

                System.out.println("data gagal: " + se);
            }
        }else if(mode.equals("pengarang")){
            try {
                PreparedStatement ps=connection.getKoneksi().prepareStatement("UPDATE t_databuku SET id_pengarang = '"+no+"' WHERE id_databuku = "+noo);
                ps.executeUpdate();
                } catch(SQLException se) {

                System.out.println("data gagal: " + se);
            }
        }
    }

    @Override
    public void deleteTransaksi(int no, String mode) {
        if(mode.equals("buku")){
            try {
               PreparedStatement ps=connection.getKoneksi().prepareStatement("DELETE FROM t_databuku WHERE no_buku = "+no);
               ps.executeUpdate();
               } catch(SQLException se) {

               System.out.println("data gagal: " + se);
           }
         }else if(mode.equals("pengarang")){
            try {
               PreparedStatement ps=connection.getKoneksi().prepareStatement("DELETE FROM t_databuku WHERE id_pengarang = "+no);
               ps.executeUpdate();
               } catch(SQLException se) {

               System.out.println("data gagal: " + se);
           }
         }
    }

    @Override
    public List<TransaksiModel> searchTransaksi(String search) {
        storageTransaksi= new ArrayList<TransaksiModel>();
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("select b.no_buku,b.nama_buku,b.isbn,b.tgl_terbit,p.id_pengarang,p.nama_pengarang,p.alamat_pengarang,d.nama_penerbit,d.alamat_penerbit,d.id_databuku,d.id from t_buku b inner join (t_databuku d inner join t_pengarang p on d.id_pengarang = p.id_pengarang) ON b.no_buku = d.no_buku WHERE d.id_databuku LIKE '%"+search+"%' || b.nama_buku LIKE '%"+search+"%' || b.isbn LIKE '%"+search+"%' || p.nama_pengarang LIKE '%"+search+"%' || b.tgl_terbit LIKE '%"+search+"%' || d.nama_penerbit LIKE '%"+search+"%'");
            ResultSet rs=ps.executeQuery();
            System.out.println("Accesss");
                while(rs.next()){
                    TransaksiModel mt=new TransaksiModel();
                    mt.setNoTransaksi(rs.getInt("id_databuku"));
                    mt.setNamaPenerbit(rs.getString("nama_penerbit")); 
                    mt.setAlamatPenerbit(rs.getString("alamat_penerbit"));
                    mt.setNoBuku(rs.getInt("no_buku"));
                    mt.setIsbnBuku(rs.getString("isbn"));
                    mt.setNamaBuku(rs.getString("nama_buku")); 
                    mt.setTglTerbit(rs.getString("tgl_terbit"));
                    mt.setNoPengarang(rs.getInt("id_pengarang"));
                    mt.setNamaPengarang(rs.getString("nama_pengarang")); 
                    mt.setAlamatPengarang(rs.getString("alamat_pengarang"));
                    mt.setNoUnix(rs.getInt("id"));

                    storageTransaksi.add(mt);
                    
                } 
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
        return storageTransaksi;
    }
    
    private int lastRecordNoTransaksi(){
       int notrans = 0;
        try {
            PreparedStatement ps=connection.getKoneksi().prepareStatement("select id_databuku as no from t_databuku ORDER BY id_databuku DESC LIMIT 1;");
            ResultSet rs=ps.executeQuery();
            rs.absolute(1);
            notrans = rs.getInt("no");
            } catch(SQLException se) {
                    
            System.out.println("data gagal: " + se);
            }
       
       return (notrans+1);
   }

    @Override
    public void updateItems(int notransaksi, int no, int nobaru, String mode) {
        if(mode.equals("buku")){
            try {
                PreparedStatement ps=connection.getKoneksi().prepareStatement("UPDATE t_databuku SET id_pengarang = "+nobaru+" WHERE id_databuku = "+notransaksi+" AND no_buku = "+no);
                ps.executeUpdate();
                } catch(SQLException se) {

                System.out.println("data gagal: " + se);
                }
        }else if(mode.equals("pengarang")){
             try {
                PreparedStatement ps=connection.getKoneksi().prepareStatement("UPDATE t_databuku SET no_buku = "+nobaru+" WHERE id_databuku = "+notransaksi+" AND id_pengarang = "+no);
                ps.executeUpdate();
                } catch(SQLException se) {

                System.out.println("data gagal: " + se);
                }
        }
    }

    @Override
    public void deleteItems(int notransaksi, int no, int noo, String mode) {
         if(mode.equals("buku")){
            try {
               PreparedStatement ps=connection.getKoneksi().prepareStatement("DELETE FROM t_databuku WHERE id_databuku = "+notransaksi+" AND no_buku = "+no+" AND id_pengarang = "+noo);
               ps.executeUpdate();
               } catch(SQLException se) {

               System.out.println("data gagal: " + se);
           }
         }else if(mode.equals("pengarang")){
            try {
               PreparedStatement ps=connection.getKoneksi().prepareStatement("DELETE FROM t_databuku WHERE id_databuku = "+notransaksi+" AND id_pengarang = "+no+" AND no_buku = "+noo);
               ps.executeUpdate();
               } catch(SQLException se) {

               System.out.println("data gagal: " + se);
           }
         }
    }

    @Override
    public void tambahItems(int no, int noo, String nama_penerbit, String alamat_penerbit,String mode) {
           if(mode.equals("buku")){
                try {
                PreparedStatement ps=connection.getKoneksi().prepareStatement("INSERT INTO t_databuku VALUES ("+this.lastRecordNoTransaksi()+","+no+","+noo+",'"+nama_penerbit+"','"+alamat_penerbit+"',"+this.lastRecordNoTransaksi()+")");
                ps.execute();
                } catch(SQLException se) {

                System.out.println("data gagal: " + se);
                }  
           }else if(mode.equals("pengarang")){
                try {
                PreparedStatement ps=connection.getKoneksi().prepareStatement("INSERT INTO t_databuku VALUES ("+this.lastRecordNoTransaksi()+","+noo+","+no+",'"+nama_penerbit+"','"+alamat_penerbit+"',"+this.lastRecordNoTransaksi()+")");
                ps.execute();
                } catch(SQLException se) {

                System.out.println("data gagal: " + se);
                }  
           }
    }
}
