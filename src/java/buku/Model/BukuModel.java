/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.Model;

/**
 *
 * @author abah
 */
public class BukuModel {
    public Integer nobuku;
    public String namabuku;
    public String isbnbuku;
    public String tglterbit;
    
    public  void setNoBuku(Integer no_buku) {
        
        nobuku = no_buku;
        
    }
    
    public Integer getNoBuku() {
        
        return nobuku;
        
    }
    
    public void setNamaBuku(String nama_buku) {
        
        namabuku = nama_buku;
        
    }
    
    public String getNamaBuku() {
        
        return namabuku;
        
    }    
    
    public void setIsbnBuku(String isbn_buku) {
        
        isbnbuku = isbn_buku;
        
    }
    
    public String getIsbnBuku() {
        
        return isbnbuku;
        
    }
    
    public void setTglTerbit(String tgl) {
        
        tglterbit = tgl;
        
    }
    
    public String getTglTerbit() {
        
        return tglterbit;
        
    }
}
