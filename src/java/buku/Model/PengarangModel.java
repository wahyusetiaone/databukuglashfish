/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.Model;

/**
 *
 * @author abah
 */
public class PengarangModel {
    private Integer nopengarang;
    private String namapengarang;
    private String alamatpengarang;
    
    public void setNoPengarang(Integer no_pengarang){
        nopengarang = no_pengarang;
    }
    
    public Integer getNoPengarang(){
        return nopengarang;
    }
    
    public void setNamaPengarang(String nama_pengarang){
        namapengarang = nama_pengarang;
    }
    
    public String getNamaPengarang(){
        return namapengarang;
    }
    
    public void setAlamatPengarang(String alamat_pengarang){
        alamatpengarang = alamat_pengarang;
    }
    
    public String getAlamatPengarang(){
        return alamatpengarang;
    }
}
