/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buku.Model;

/**
 *
 * @author abah
 */
public class TransaksiModel {
    public Integer unix;
    public Integer notr;
    public String namapenerbit;
    public String alamatpenerbit;
    public Integer nobuku;
    public String namabuku;
    public String isbnbuku;
    public String tglterbit;
    public Integer nopengarang;
    public String namapengarang;
    public String alamatpengarang;
    
    public  void setNoUnix(Integer id) {
        
        unix = id;
        
    }
    
    public Integer getNoUnix() {
        
        return unix;
        
    }
    public  void setNoTransaksi(Integer id_transaksi) {
        
        notr = id_transaksi;
        
    }
    
    public Integer getNoTransaksi() {
        
        return notr;
        
    }
    
    public void setNamaPenerbit(String nama_penerbit) {
        
        namapenerbit = nama_penerbit;
        
    }
    
    public String getNamaPenerbit() {
        
        return namapenerbit;
        
    }    
    
    public void setAlamatPenerbit(String alamat_penerbit) {
        
        alamatpenerbit = alamat_penerbit;
        
    }
    
    public String getAlamatPenerbit() {
        
        return alamatpenerbit;
        
    }
    
    public  void setNoBuku(Integer no_buku) {
        
        nobuku = no_buku;
        
    }
    
    public Integer getNoBuku() {
        
        return nobuku;
        
    }
    
    public void setNamaBuku(String nama_buku) {
        
        namabuku = nama_buku;
        
    }
    
    public String getNamaBuku() {
        
        return namabuku;
        
    }    
    
    public void setIsbnBuku(String isbn_buku) {
        
        isbnbuku = isbn_buku;
        
    }
    
    public String getIsbnBuku() {
        
        return isbnbuku;
        
    }
    
    public void setTglTerbit(String tgl) {
        
        tglterbit = tgl;
        
    }
    
    public String getTglTerbit() {
        
        return tglterbit;
        
    }
    
    public void setNoPengarang(Integer no_pengarang){
        nopengarang = no_pengarang;
    }
    
    public Integer getNoPengarang(){
        return nopengarang;
    }
    
    public void setNamaPengarang(String nama_pengarang){
        namapengarang = nama_pengarang;
    }
    
    public String getNamaPengarang(){
        return namapengarang;
    }
    
    public void setAlamatPengarang(String alamat_pengarang){
        alamatpengarang = alamat_pengarang;
    }
    
    public String getAlamatPengarang(){
        return alamatpengarang;
    }
}
