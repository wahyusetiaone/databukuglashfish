-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 19, 2019 at 10:02 PM
-- Server version: 5.7.21-1
-- PHP Version: 7.2.4-1+b1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `data_buku`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `ps_latihan` (OUT `out_nama_buku` VARCHAR(50), IN `in_no_buku` INT)  BEGIN
select nama_buku into out_nama_buku from t_buku where no_buku = in_no_buku;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_json` (IN `nobuku` INT, OUT `data_out` JSON)  BEGIN

    SELECT JSON_MERGE(
       JSON_OBJECT('jumlah_data', (SELECT COUNT(no_buku) FROM t_buku)),
       JSON_OBJECT('data_diambil', (SELECT COUNT(no_buku) FROM t_buku where no_buku = nobuku)),
       JSON_OBJECT('nama_data_diambil', (SELECT nama_buku FROM t_buku where no_buku = nobuku))
    ) INTO data_out;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jsonarray` (IN `nobuku` INT, OUT `data_out` JSON)  BEGIN

    SELECT JSON_ARRAY(
       (SELECT COUNT(no_buku) FROM t_buku),
       (SELECT COUNT(no_buku) FROM t_buku where no_buku = nobuku),
       (SELECT nama_buku FROM t_buku where no_buku = nobuku)
    ) INTO data_out;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_queryjson` (`json_query` JSON)  BEGIN
DECLARE i_nobuku INT default null;
DECLARE i_namabuku varchar(255) default null;

set i_nobuku = JSON_UNQUOTE(JSON_EXTRACT(json_query,'$.nobuku'));
set i_namabuku = JSON_EXTRACT(json_query,'$.namabuku');

 if (i_nobuku is not null) && (i_namabuku is not null) then
   select isbn, tgl_terbit 
   from t_buku 
   where no_buku = i_nobuku and nama_buku = i_namabuku 
   group by no_buku; 
 elseif (i_nobuku is not null) then
   select nama_buku, isbn, tgl_terbit 
   from t_buku 
   where no_buku = i_nobuku
   group by nama_buku; 
 elseif (i_namabuku is not null) then
   select no_buku, isbn, tgl_terbit 
   from t_buku 
   where nama_buku = i_namabuku
   group by no_buku;
 else
   select null as 'no input';
 end if; 

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `t_buku`
--

CREATE TABLE `t_buku` (
  `no_buku` int(11) NOT NULL,
  `nama_buku` varchar(50) NOT NULL,
  `isbn` varchar(20) NOT NULL,
  `tgl_terbit` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_buku`
--

INSERT INTO `t_buku` (`no_buku`, `nama_buku`, `isbn`, `tgl_terbit`) VALUES
(1001, 'Buku IPA III', '2020ABCD2020', '2019-06-19'),
(1002, 'Buku MATEMATIKA I', '2020GMBD2020', '2019-06-19'),
(1003, 'Buku IPS I', '2030GAHS2030', '2019-06-16'),
(1004, 'Buku AGAMA', '2432432423423', '2019-06-21'),
(1005, 'Buku IPS Ekonomi II', '234342342353', '2019-06-21'),
(1006, 'Buku IPS II', '2022JJMC2817', '2019-06-20'),
(1007, 'Buku IPS III', '3392KSBN3213', '2019-06-01'),
(1008, 'Buku TIK', '3112JDNL9843', '2019-06-30'),
(1009, 'Buku IPA I', '3342NDHB3271', '2019-06-23'),
(1010, 'Buku IPA II', '3648JSGG3827', '2019-06-12'),
(1011, 'Buku MATEMATIKA II', '3134JABW3826', '2019-06-08'),
(1012, 'Buku KIMIA', '2918SBHM3726', '2019-06-02'),
(1013, 'Buku FISIKA', '2918ADSH2122', '2019-06-01'),
(1014, 'Buku KIMIA I', '3322JDNH3335', '2019-06-09'),
(1015, 'Buku FISIKA I', '2717GAJA8372', '2019-06-10'),
(1016, 'Buku GEO', '1233HSBD2938', '2019-06-20');

-- --------------------------------------------------------

--
-- Table structure for table `t_databuku`
--

CREATE TABLE `t_databuku` (
  `id_databuku` int(11) NOT NULL,
  `no_buku` int(11) NOT NULL,
  `id_pengarang` int(11) NOT NULL,
  `nama_penerbit` varchar(20) DEFAULT NULL,
  `alamat_penerbit` varchar(50) DEFAULT NULL,
  `id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_databuku`
--

INSERT INTO `t_databuku` (`id_databuku`, `no_buku`, `id_pengarang`, `nama_penerbit`, `alamat_penerbit`, `id`) VALUES
(3008, 1002, 2001, 'MEDAN', 'WNG', 3008),
(3011, 1002, 2008, 'MEDIA KARYA ABADI', 'SOLO KOTA', 3011),
(3012, 1003, 2008, 'MEDIA KARYA ABADI', 'SOLO KOTA', 3012),
(3014, 1010, 2009, 'KARYA BANGSA CITA', 'KARTASURA', 3014),
(3016, 1015, 2009, 'SINAR', 'SALATIGA', 3016),
(3017, 1001, 2013, 'MEDIA AGUNG', 'JAKARATA', 3017),
(3018, 1001, 2012, 'MEDIA AGUNG', 'JAKARATA', 3018),
(3019, 1016, 2004, 'MEDIA KARYA ABADI II', 'SURAKARTA', 3019),
(3020, 1016, 2013, 'MEDIA KARYA ABADI II', 'SURAKARTA', 3020),
(3021, 1016, 2014, 'MEDIA KARYA ABADI II', 'SURAKARTA', 3021),
(3022, 1015, 2001, 'SURYA CITRA', 'SALATIGA', 3022),
(3023, 1014, 2001, 'SURYA CITRA', 'SALATIGA', 3023),
(3025, 1010, 2008, 'KARYA BANGSA CITA', 'KARTASURA', 3025),
(3026, 1010, 2001, 'KARTASURA', 'KARTASURA', 3026);

-- --------------------------------------------------------

--
-- Table structure for table `t_pengarang`
--

CREATE TABLE `t_pengarang` (
  `id_pengarang` int(11) NOT NULL,
  `nama_pengarang` varchar(20) NOT NULL,
  `alamat_pengarang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_pengarang`
--

INSERT INTO `t_pengarang` (`id_pengarang`, `nama_pengarang`, `alamat_pengarang`) VALUES
(2001, 'Wahyu Setiawan', 'Wonogiri'),
(2002, 'Abah Setiawan', 'Surakarta'),
(2003, 'Paijo Setiawan ', 'Yogyakarta'),
(2004, 'Milea', 'Sukoharjo'),
(2005, 'Paijan', 'Surakarta'),
(2006, 'Sugino', 'Solo '),
(2007, 'Sulio', 'Sukoharjo'),
(2008, 'Sugo', 'Sala'),
(2009, 'Julia', 'Surakarta'),
(2010, 'Semi', 'Jogjakarta'),
(2011, 'Samin', 'Semarang'),
(2012, 'Gino', 'Salatiga'),
(2013, 'Pajar', 'Tulung Agung'),
(2014, 'Sukijem', 'Grogol');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_buku`
--
ALTER TABLE `t_buku`
  ADD PRIMARY KEY (`no_buku`);

--
-- Indexes for table `t_databuku`
--
ALTER TABLE `t_databuku`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lnk_t_buku_t_databuku` (`no_buku`),
  ADD KEY `lnk_t_pengarang_t_databuku` (`id_pengarang`);

--
-- Indexes for table `t_pengarang`
--
ALTER TABLE `t_pengarang`
  ADD PRIMARY KEY (`id_pengarang`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_databuku`
--
ALTER TABLE `t_databuku`
  ADD CONSTRAINT `lnk_t_buku_t_databuku` FOREIGN KEY (`no_buku`) REFERENCES `t_buku` (`no_buku`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lnk_t_pengarang_t_databuku` FOREIGN KEY (`id_pengarang`) REFERENCES `t_pengarang` (`id_pengarang`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
