<%-- 
    Document   : editbuku.jsp
    Created on : Jun 10, 2019, 10:46:54 PM
    Author     : abah
--%>

<%@page import="buku.Model.PengarangModel"%>
<%@page import="buku.Model.BukuModel"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="buku.Model.TransaksiModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Thema Content Managemen System</title>
    <link rel="stylesheet" type="text/css" href="asset/css/setone.css">
    <link rel="stylesheet" type="text/css" href="asset/css/form.css">
    <link rel="stylesheet" type="text/css" href="asset/css/modal.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <header>
        <div class="head">
            <div class="kiri-head">
                <img id="img-mobile" src="asset/images/wordmark4.png" alt="none">
            </div>
            <div class="kanan-head">
                <div class="button-div">
                    <img src="asset/images/avatar/ava2.jpg" alt="">
                    Wahyu
                </div>
            </div>
        </div>
    </header>
    <nav>
        <aside id="menu-mobile" class="menu-mobile">
            <ul>
                <li><a href="index.html">Dashboard</a></li>
                <li><a href="form.html">Transaksi</a></li>
                <li><a href="">Buku</a></li>
                <li><a href="">Pengarang</a></li>
                <li><a href="">Pengaturan</a></li>
            </ul>
        </aside>
        <aside id="menu" class="menu">
            <div class="head-img">
                <img id="head-img" src="asset/images/wordmark4.png" alt="none">
            </div>
            <ul>
                <li>
                    <a href="/BukuGlassFish/">
                        <img class="icon-menu" src="asset/images/icon/home.png" alt="none">
                        <span>Dashboard</span>
                    </a>
                </li
                <hr>
                <li>
                    <a href="/BukuGlassFish/transaksi?sort=buku">
                        <img class="icon-menu" src="asset/images/icon/apotik.png" alt="none">
                        <span>Transaksi</span>
                    </a>
                </li>
                <hr>
                <li>
                    <a href="/BukuGlassFish/buku">
                        <img class="icon-menu" src="asset/images/icon/kamar.png" alt="none">
                        <span>Buku</span>
                    </a>
                </li>
                <hr>
                <li>
                    <a href="/BukuGlassFish/pengarang">
                        <img class="icon-menu" src="asset/images/icon/petugas.png" alt="none">
                        <span>Pengarang</span>
                    </a>
                </li>
                <hr>
            </ul>
            <br>
            <br>
            <img id="button" class="panah" src="asset/images/icon/kiri.png" alt="none">
        </aside>
    </nav>
    <section>
        <article id="isi" class="isi">
            <div class="grid-container">
                <div class="kolom">
                    <div class="box">
                        <div class="box-container judul">
                            <h4><b>Details Transaksi</b></h4>
                        </div>
                    </div>
                </div>
            </div>
            <%
            String mode = request.getParameter("mode");
            if(mode.equals("buku")){
            %>
            <div class="grid-container">
                <div class="baris">
                    <div class="kolom">
                        <div class="box">
                            <%
                                    List<TransaksiModel> buuk = (ArrayList) request.getAttribute("data");
                                    int count = buuk.size();
                                    while(count%3 != 0){
                                        count+=1;
                                    };
                                    int loop = count/3;
                             %>
                            <div>
                                <span onclick="tambahItems('<%=buuk.get(0).getNoBuku()%>','<%=buuk.get(0).getNamaPenerbit()%>','<%=buuk.get(0).getAlamatPenerbit()%>','buku')" class="fa-stack fa-lg" style="float: right">
                                    <i class="fa blue fa-circle fa-stack-2x"></i>
                                    <i class="fa blue fa-plus fa-stack-1x fa-inverse"></i>
                                </span> 
                                <span onclick="mDelete('buku','<%=buuk.get(0).getNoTransaksi()%>','<%=buuk.get(0).getNoBuku()%>','0','main')" class="fa-stack fa-lg" style="float: right">
                                    <i class="fa red fa-circle fa-stack-2x"></i>
                                    <i class="fa red fa-trash fa-stack-1x fa-inverse"></i>
                                </span> 
                                <span onclick="editMain('<%=buuk.get(0).getNoTransaksi()%>','<%=buuk.get(0).getNoBuku()%>','buku')" class="fa-stack fa-lg" style="float: right;">
                                    <i class="fa green fa-circle fa-stack-2x"></i>
                                    <i class="fa green fa-pencil fa-stack-1x fa-inverse"></i>
                                </span> 
                            </div>
                             
                            <div class="box-container judul">
                                <h4><b>Nomor ISBN BUKU : <% out.print(buuk.get(0).getIsbnBuku()); %> </b></h4>
                            </div>
                            <div class="box-container text-kiri">
                                <label for="foobat">Nama Buku : </label>
                                <span class="form-output"> <% out.print(buuk.get(0).getNamaBuku()); %> </span>
                                <label for="foobat">Nama Penerbit : </label>
                                <span class="form-output"> <% out.print(buuk.get(0).getNamaPenerbit()); %> </span>
                                <label for="foobat">Alamat Penerbit : </label>
                                <span class="form-output"> <% out.print(buuk.get(0).getAlamatPenerbit()); %> </span>
                                <label for="fotglpembuatan">Tanggal diterbitkan : </label>
                                <span class="form-output"> <% out.print(buuk.get(0).getTglTerbit()); %> </span>
                            </div>
                        </div>
                    </div>
                </div>
                <%
                    for(int y=1;y<=loop;y++){
                %>
                <div class="baris">
                <%
                    int z=y*3-3;
                    int w=y*3;
                    for (int x=z;x<w;x++){
                    if(x >= buuk.size()){
                %>
                    <div class="kolom">
                        <div class="box" style="height: 215px;">
                        </div>
                    </div>   
                <%
                    }else{
                %>
                    <div class="kolom">
                        <div class="box">
                            <div class="box-container judul">
                                <h4><b>No Pengarang : <% out.print(buuk.get(x).getNoPengarang()); %> </b></h4>
                            </div>
                            <div class="box-container text-kiri">
                                <label for="foobat">Nama Pengarang : </label>
                                <span class="form-output"> <% out.print(buuk.get(x).getNamaPengarang()); %> </span>
                                <label for="fotglpembuatan">Alamat Pengarang : </label>
                                <span class="form-output"> <% out.print(buuk.get(x).getAlamatPengarang()); %> </span>
                            </div>
                            <span onclick="mDelete('buku','<%=buuk.get(x).getNoTransaksi()%>','<%=buuk.get(x).getNoBuku()%>','<%=buuk.get(x).getNoPengarang()%>','items')"  class="fa-stack fa-lg" style="float: right">
                                <i class="fa red fa-circle fa-stack-2x"></i>
                                <i class="fa red fa-trash fa-stack-1x fa-inverse"></i>
                            </span> 
                            <span onclick="editItems('<%=buuk.get(x).getNoTransaksi()%>','<%=buuk.get(x).getNoPengarang()%>','<%=buuk.get(x).getNoBuku()%>','buku')" class="fa-stack fa-lg" style="float: right;">
                                <i class="fa green fa-circle fa-stack-2x"></i>
                                <i class="fa green fa-pencil fa-stack-1x fa-inverse"></i>
                            </span>
                        </div>
                    </div> 
                <%}%>
                <%}%>
                <%}%>
                </div>
            </div>
            <%
                }else if(mode.equals("pengarang")){
            %>
            <div class="grid-container">
                <div class="baris">
                    <div class="kolom">
                        <div class="box">
                            <%
                                    List<TransaksiModel> buuk = (ArrayList) request.getAttribute("data");
                                    int count = buuk.size();
                                    while(count%3 != 0){
                                        count+=1;
                                    };
                                    int loop = count/3;
                             %>
                            <div>
                            <span onclick="tambahItems('<%=buuk.get(0).getNoPengarang()%>','<%=buuk.get(0).getNamaPenerbit()%>','<%=buuk.get(0).getAlamatPenerbit()%>','pengarang')"  class="fa-stack fa-lg" style="float: right">
                                <i class="fa blue fa-circle fa-stack-2x"></i>
                                <i class="fa blue fa-plus fa-stack-1x fa-inverse"></i>
                            </span> 
                            <span onclick="mDelete('pengarang','<%=buuk.get(0).getNoTransaksi()%>','<%=buuk.get(0).getNoPengarang()%>','0','main')" class="fa-stack fa-lg" style="float: right">
                                <i class="fa red fa-circle fa-stack-2x"></i>
                                <i class="fa red fa-trash fa-stack-1x fa-inverse"></i>
                            </span> 
                                <span onclick="editMain('<%=buuk.get(0).getNoTransaksi()%>','<%=buuk.get(0).getNoPengarang()%>','pengarang')" class="fa-stack fa-lg" style="float: right;">
                                <i class="fa green fa-circle fa-stack-2x"></i>
                                <i class="fa green fa-pencil fa-stack-1x fa-inverse"></i>
                            </span> 
                            </div>
                            <div class="box-container judul">
                                <h4><b>Nomor Pengarang BUKU : <% out.print(buuk.get(0).getNoPengarang()); %> </b></h4>
                            </div>
                            <div class="box-container text-kiri">
                                <label for="foobat">Nama Pengarang : </label>
                                <span class="form-output"> <% out.print(buuk.get(0).getNamaPengarang()); %> </span>
                                <label for="foobat">Alamat Pengarang : </label>
                                <span class="form-output"> <% out.print(buuk.get(0).getAlamatPengarang()); %> </span>
                            </div>
                        </div>
                    </div>
                </div>
                <%
                    for(int y=1;y<=loop;y++){
                %>
                <div class="baris">
                <%
                    int z=y*3-3;
                    int w=y*3;
                    for (int x=z;x<w;x++){
                    if(x >= buuk.size()){
                %>
                    <div class="kolom">
                        <div class="box" style="height: 215px;">
                        </div>
                    </div>   
                <%
                    }else{
                %>
                    <div class="kolom">
                        <div class="box">
                            <div class="box-container judul">
                                <h4><b>No ISBN : <% out.print(buuk.get(x).getIsbnBuku()); %> </b></h4>
                            </div>
                            <div class="box-container text-kiri">
                                <label for="foobat">Nama Buku : </label>
                                <span class="form-output"> <% out.print(buuk.get(x).getNamaBuku()); %> </span>
                                <label for="fotglpembuatan">Tanggal Terbit : </label>
                                <span class="form-output"> <% out.print(buuk.get(x).getTglTerbit()); %> </span>
                            </div>
                            <span onclick="mDelete('pengarang','<%=buuk.get(x).getNoTransaksi()%>','<%=buuk.get(x).getNoPengarang()%>','<%=buuk.get(x).getNoBuku()%>','items')" class="fa-stack fa-lg" style="float: right">
                                <i class="fa red fa-circle fa-stack-2x"></i>
                                <i class="fa red fa-trash fa-stack-1x fa-inverse"></i>
                            </span> 
                            <span onclick="editItems('<%=buuk.get(x).getNoTransaksi()%>','<%=buuk.get(x).getNoBuku()%>','<%=buuk.get(x).getNoPengarang()%>','pengarang')" class="fa-stack fa-lg" style="float: right;">
                                <i class="fa green fa-circle fa-stack-2x"></i>
                                <i class="fa green fa-pencil fa-stack-1x fa-inverse"></i>
                            </span> 
                        </div>
                    </div> 
                <%}%>
                <%}%>
                <%}%>
                </div>
            </div>
            <%
            }
            %>
    <%
        List<TransaksiModel> buuk = (ArrayList) request.getAttribute("data");
    %>
        <!--mainmodal-->
        <div id="main" class="w3-modal">
          <div class="w3-modal-content w3-animate-left w3-card-4">
            <header class="w3-container w3-teal"> 
              <span onclick="document.getElementById('main').style.display='none'" 
              class="w3-button w3-display-topright">&times;</span>
              <h2>Edit Transaksi</h2>
            </header>
            <div class="w3-container">
            <%
                if(mode.equals("buku")){
            %>
            <br>
            <form method="post" action="/BukuGlassFish/transaksi?post=editmain">
                    <input type="hidden" name="mode" value="<% out.print(mode); %>">

                    <%
                            List<TransaksiModel> tr = (ArrayList) request.getAttribute("data");
                            for (int g=0;g<tr.size();g++){
                    %>
                    <input type="hidden" name="noo[<% out.print(g); %>]" value="<% out.print(tr.get(g).getNoTransaksi()); %>">                        
                    <%}%>
                    <label for="fobat">Nama Buku</label>
                    <select name="no">
                        <%
                            List<BukuModel> dbuku = (ArrayList) request.getAttribute("dbuku");
                            for (BukuModel buku : dbuku){
                        %>
                        <option <% if(buku.getNoBuku().equals(buuk.get(0).getNoBuku())){out.print("selected");} %> value="<%=buku.getNoBuku()%>"><%=buku.getNamaBuku()%></option>
                        <%}%>
                      </select>
                    <label for="fobat">Nama Penerbit</label>
                    <input type="text" id="namapenerbit" name="namapenerbit" placeholder="Penerbit.." value="<% out.print(buuk.get(0).getNamaPenerbit()); %>">
                    <label for="fobat">Alamat Penerbit</label>
                    <input type="text" id="alamatpennerbit" name="alamatpenerbit" placeholder="Penerbit.." value="<% out.print(buuk.get(0).getAlamatPenerbit()); %>">
                    <label for="ftgl">Tgl diterbitkan <span class="note"> Tanggal Min 10 Maret 2019 </span></label>
                    <input type="date" id="tglterbit" name="tglterbit" min="2019-03-10" placeholder="Tanggal Terbit Buku.." value="<% out.print(buuk.get(0).getTglTerbit()); %>">
                    <input type="submit" value="Submit">
            </form>
            <%
                }else if(mode.equals("pengarang")){
            %>
            <br>
            <form method="post" action="/BukuGlassFish/transaksi?post=editmain">
                    <input type="hidden" name="mode" value="<% out.print(mode); %>">

                    <%
                            List<TransaksiModel> tr = (ArrayList) request.getAttribute("data");
                            for (int g=0;g<tr.size();g++){
                    %>
                    <input type="hidden" name="noo[<% out.print(g); %>]" value="<% out.print(tr.get(g).getNoTransaksi()); %>">                        
                    <%}%>
                    <label for="fobat">Nama Pengarang</label>
                    <select name="no">
                        <%
                            List<PengarangModel> dpeng = (ArrayList) request.getAttribute("dpengarang");
                            for (PengarangModel peng : dpeng){
                        %>
                        <option <% if(peng.getNoPengarang().equals(buuk.get(0).getNoPengarang())){out.print("selected");} %> value="<%=peng.getNoPengarang()%>"><%=peng.getNamaPengarang()%></option>
                        <%}%>
                      </select>
                    <input type="submit" value="Submit">
            </form>
            <%
            }
            %>
            </div>
            <footer class="w3-container w3-teal">
                <p><i>Mohon perhatikan dengan teliti saat memasukan data agar informasi tetap terjaga kebenaranya !!!</i></p>
            </footer>
          </div>
        </div>
        
        <!--itmsmodal-->
        <div id="items" class="w3-modal">
          <div class="w3-modal-content w3-animate-left w3-card-4">
            <header class="w3-container w3-teal"> 
              <span onclick="document.getElementById('items').style.display='none'" 
              class="w3-button w3-display-topright">&times;</span>
              <h2>Form Edit Data</h2>
            </header>
            <div class="w3-container" id="editItemModal">
            </div>
            <footer class="w3-container w3-teal">
              <p>Modal Footer</p>
            </footer>
          </div>
        </div>
        
        <div id="tambahitems" class="w3-modal">
          <div class="w3-modal-content w3-animate-left w3-card-4">
            <header class="w3-container w3-teal"> 
              <span onclick="document.getElementById('tambahitems').style.display='none'" 
              class="w3-button w3-display-topright">&times;</span>
              <h2 style="text-align: center;">Form Tambah Data</h2>
            </header>
            <div class="w3-container" id="tambahItemModal">
            </div>
            <footer class="w3-container w3-teal">
              <p>Modal Footer</p>
            </footer>
          </div>
        </div>
         
        </article>
    </section>
    <footer>
        <div id="foot" class="foot">
            <div class="identitas">
                <strong>Wahyu Setiawan || 17.5.00173</strong>
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="asset/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="asset/js/sidebar.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        function tambahItems(no,namapenerbit,alamatpenerbit,mode){
            if(mode == "buku"){
                document.getElementById('tambahItemModal').innerHTML = ""
                document.getElementById('tambahitems').style.display='block';

                var f = document.createElement("form");
                f.setAttribute('method',"post");
                f.setAttribute('action',"/BukuGlassFish/transaksi?post=tambahitem");

                var para = document.createElement("p");
                var node = document.createTextNode("Pilih nomor pengarang !!!");
                para.appendChild(node)

                var a = document.createElement("input"); //input element, Submit button
                a.setAttribute('type',"hidden");
                a.setAttribute('name',"no");
                a.setAttribute('value',no);

                var b = document.createElement("input"); //input element, Submit button
                b.setAttribute('type',"hidden");
                b.setAttribute('name',"namapenerbit");
                b.setAttribute('value',namapenerbit);

                var c = document.createElement("input"); //input element, Submit button
                c.setAttribute('type',"hidden");
                c.setAttribute('name',"alamatpenerbit");
                c.setAttribute('value',alamatpenerbit);    

                var selected = document.createElement("select"); //input element, Submit button
                selected.setAttribute('name',"noo");
                <%
                                List<PengarangModel> ddpengarang = (ArrayList) request.getAttribute("dpengarang");
                                for (PengarangModel peng : ddpengarang){
                %>            
                    var opt = document.createElement('option');
                    opt.value = '<%= peng.getNoPengarang() %>';
                    opt.innerHTML = '<%= peng.getNamaPengarang() %>';
                    selected.appendChild(opt);
                <%}%>
                var s = document.createElement("input"); //input element, Submit button
                s.setAttribute('type',"submit");
                s.setAttribute('value',"Submit");
                var z = document.createElement("input"); //input element, Submit button
                z.setAttribute('type',"hidden");
                z.setAttribute('name',"mode");
                z.setAttribute('value',mode);
                f.appendChild(para);
                f.appendChild(selected);
                f.appendChild(a);
                f.appendChild(b);
                f.appendChild(c);
                f.appendChild(z);
                f.appendChild(s);

                document.getElementById('tambahItemModal').appendChild(f);
            }else if(mode = "pengarang"){
                document.getElementById('tambahItemModal').innerHTML = ""
                document.getElementById('tambahitems').style.display='block';

                var f = document.createElement("form");
                f.setAttribute('method',"post");
                f.setAttribute('action',"/BukuGlassFish/transaksi?post=tambahitem");

                var para = document.createElement("p");
                var node = document.createTextNode("Pilih Buku !!!");
                para.appendChild(node)

                var a = document.createElement("input"); //input element, Submit button
                a.setAttribute('type',"hidden");
                a.setAttribute('name',"no");
                a.setAttribute('value',no);

                var b = document.createElement("input"); //input element, Submit button
                b.setAttribute('type',"hidden");
                b.setAttribute('name',"namapenerbit");
                b.setAttribute('value',namapenerbit);

                var c = document.createElement("input"); //input element, Submit button
                c.setAttribute('type',"hidden");
                c.setAttribute('name',"alamatpenerbit");
                c.setAttribute('value',alamatpenerbit);    

                var selected = document.createElement("select"); //input element, Submit button
                selected.setAttribute('name',"noo");
                <%
                                List<BukuModel> ddbuku = (ArrayList) request.getAttribute("dbuku");
                                for (BukuModel peng : ddbuku){
                %>            
                    var opt = document.createElement('option');
                    opt.value = '<%= peng.getNoBuku() %>';
                    opt.innerHTML = '<%= peng.getNamaBuku() %>';
                    selected.appendChild(opt);
                <%}%>
                var s = document.createElement("input"); //input element, Submit button
                s.setAttribute('type',"submit");
                s.setAttribute('value',"Submit");
                var z = document.createElement("input"); //input element, Submit button
                z.setAttribute('type',"hidden");
                z.setAttribute('name',"mode");
                z.setAttribute('value',mode);
                f.appendChild(para);
                f.appendChild(selected);
                f.appendChild(a);
                f.appendChild(b);
                f.appendChild(c);
                f.appendChild(z);
                f.appendChild(s);

                document.getElementById('tambahItemModal').appendChild(f);
            }
        }
        
        function editItems(notr,no,noo,mode){
            if(mode=="buku"){
                document.getElementById('editItemModal').innerHTML = ""
                document.getElementById('items').style.display='block';
                var a = document.createElement("input"); //input element, Submit button
                a.setAttribute('type',"hidden");
                a.setAttribute('name',"notr");
                a.setAttribute('value',notr);
                
                var b = document.createElement("input"); //input element, Submit button
                b.setAttribute('type',"hidden");
                b.setAttribute('name',"no");
                b.setAttribute('value',noo);
                
                var c = document.createElement("input"); //input element, Submit button
                c.setAttribute('type',"hidden");
                c.setAttribute('name',"mode");
                c.setAttribute('value',mode);
                
                var f = document.createElement("form");
                f.setAttribute('method',"post");
                f.setAttribute('action',"/BukuGlassFish/transaksi?post=edititem");

                var para = document.createElement("p");
                var node = document.createTextNode("Pilih nomor pengarang !!!");
                para.appendChild(node)

                var selected = document.createElement("select"); //input element, Submit button
                selected.setAttribute('name',"nobaru");

                <%
                                List<PengarangModel> dpengarang = (ArrayList) request.getAttribute("dpengarang");
                                for (PengarangModel peng : dpengarang){
                %>            
                    var opt = document.createElement('option');
                    opt.value = '<%= peng.getNoPengarang() %>';
                    opt.innerHTML = '<%= peng.getNamaPengarang() %>';
                    var nilai = <%= peng.getNoPengarang() %>;
                    if(no == nilai){
                        opt.setAttribute("selected", "selected");
                    }
                    selected.appendChild(opt);
                <%}%>
                var s = document.createElement("input"); //input element, Submit button
                s.setAttribute('type',"submit");
                s.setAttribute('value',"Perbarui");

                f.appendChild(para);
                f.appendChild(selected);
                f.appendChild(a);
                f.appendChild(b);
                f.appendChild(c);
                f.appendChild(s);

                //and some more input elements here
                //and dont forget to add a submit button

                document.getElementById('editItemModal').appendChild(f);
            }else if(mode == "pengarang"){
                document.getElementById('editItemModal').innerHTML = ""
                document.getElementById('items').style.display='block';
                var a = document.createElement("input"); //input element, Submit button
                a.setAttribute('type',"hidden");
                a.setAttribute('name',"notr");
                a.setAttribute('value',notr);
                
                var b = document.createElement("input"); //input element, Submit button
                b.setAttribute('type',"hidden");
                b.setAttribute('name',"no");
                b.setAttribute('value',noo);
                
                var c = document.createElement("input"); //input element, Submit button
                c.setAttribute('type',"hidden");
                c.setAttribute('name',"mode");
                c.setAttribute('value',mode);
                
                var f = document.createElement("form");
                f.setAttribute('method',"post");
                f.setAttribute('action',"/BukuGlassFish/transaksi?post=edititem");

                var para = document.createElement("p");
                var node = document.createTextNode("Pilih Buku !!!");
                para.appendChild(node)

                var selected = document.createElement("select"); //input element, Submit button
                selected.setAttribute('name',"nobaru");

                <%
                                List<BukuModel> dbu = (ArrayList) request.getAttribute("dbuku");
                                for (BukuModel db : dbu){
                %>            
                    var opt = document.createElement('option');
                    opt.value = '<%= db.getNoBuku() %>';
                    opt.innerHTML = '<%= db.getNamaBuku() %>';
                    var nilai = <%= db.getNoBuku() %>;
                    if(no == nilai){
                        opt.setAttribute("selected", "selected");
                    }
                    selected.appendChild(opt);
                <%}%>
                var s = document.createElement("input"); //input element, Submit button
                s.setAttribute('type',"submit");
                s.setAttribute('value',"Perbarui");

                f.appendChild(para);
                f.appendChild(selected);
                f.appendChild(a);
                f.appendChild(b);
                f.appendChild(c);
                f.appendChild(s);

                //and some more input elements here
                //and dont forget to add a submit button

                document.getElementById('editItemModal').appendChild(f);
            }
        }
        function editMain(no,mode){
            document.getElementById('main').style.display='block';
        }
        
        function mDelete(mode,notr,no,noo,what){
           if(mode == 'buku' || mode == 'pengarang'){
               if(what == 'main'){
                    swal({
                        title: "Apa Kamu Yakin?",
                        text: "Tindakan ini akan menghapus Transaksi !!!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          $.ajax({
                                type: 'POST',
                                url: 'http://localhost:8080/BukuGlassFish/transaksi?post=deletemain',
                                data: {
                                    'no': no,
                                    'mode' : mode
                                },
                                dataType : 'json',
                                success:function(data, status, xhttp){
                                    console.log("got response as "+data);
                                    if(data['kode'] == 200){
                                        swal("Berhasi di Transaksi !!!", "Transaksi telah berhasil di hapus !", "success").then(function(){
                                            window.location.href = data['redirect'];
                                        });
                                    }
                                }
                          });
                        } else {
                          swal("Membatalkan penghapusan transaksi!");
                        }
                      });
                }else if(what == 'items'){
                    swal({
                        title: "Apa Kamu Yakin?",
                        text: "Tindakan ini akan menghapus Pengarang !!!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          $.ajax({
                                type: 'POST',
                                url: 'http://localhost:8080/BukuGlassFish/transaksi?post=deleteitem',
                                data: {
                                    'mode' : mode,
                                    'notr': notr,
                                    'no' : no,
                                    'noo' : noo
                                },
                                dataType : 'json',
                                success:function(data, status, xhttp){
                                    console.log("got response as "+data);
                                    if(data['kode'] == 200){
                                        swal("Berhasi di Pengarang !!!", "Pengarang telah berhasil di hapus !", "success").then(function(){
                                            location.reload();
                                        });
                                    }
                                }
                          });
                        } else {
                          swal("Membatalkan penghapusan pengarang!");
                        }
                      });
               }
           }
        }
    </script>
</body>

</html>