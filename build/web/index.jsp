<%-- 
    Document   : index
    Created on : Jun 5, 2019, 12:02:27 PM
    Author     : abah
--%>

<%@page import="buku.Model.PengarangModel"%>
<%@page import="buku.Implementasi.PengarangImplDAO"%>
<%@page import="buku.Implementasi.BukuImplDAO"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="buku.Model.BukuModel"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Thema Content Managemen System</title>
    <link rel="stylesheet" type="text/css" href="asset/css/setone.css">    
    <link rel="stylesheet" type="text/css" href="asset/css/form.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="asset/css/modal.css">

    <style type="text/css">
        .bariskhusus { 
            display:flex; 
            justify-content:space-between;
            width: 65vmax; 
            height:30vmax;
            border: 1px solid black;
          }
          .boxkhusus { 
            flex: 0 0 40%;
            height: 100%; 
            text-align:center; 
            border: 1px solid black;
          }
    </style>
</head>

<body>
    <header>
        <div class="head">
            <div class="kiri-head">
                <img id="img-mobile" src="asset/images/wordmark4.png" alt="none">
            </div>
            <div class="kanan-head">
                <div class="button-div">
                    <img src="asset/images/avatar/ava2.jpg" alt="">
                    Wahyu
                </div>
            </div>
        </div>
    </header>
    <nav>
        <aside id="menu-mobile" class="menu-mobile">
            <ul>
                <li><a href="index.html">Dashboard</a></li>
                <li><a href="form.html">Transaksi</a></li>
                <li><a href="">Buku</a></li>
                <li><a href="">Pengarang</a></li>
                <li><a href="">Pengaturan</a></li>
            </ul>
        </aside>
        <aside id="menu" class="menu">
            <div class="head-img">
                <img id="head-img" src="asset/images/wordmark4.png" alt="none">
            </div>
            <ul>
                <li>
                    <a href="/BukuGlassFish/">
                        <img class="icon-menu" src="asset/images/icon/home.png" alt="none">
                        <span>Dashboard</span>
                    </a>
                </li
                <hr>
                <li>
                    <a href="/BukuGlassFish/transaksi?sort=buku">
                        <img class="icon-menu" src="asset/images/icon/apotik.png" alt="none">
                        <span>Transaksi</span>
                    </a>
                </li>
                <hr>
                <li>
                    <a href="/BukuGlassFish/buku">
                        <img class="icon-menu" src="asset/images/icon/kamar.png" alt="none">
                        <span>Buku</span>
                    </a>
                </li>
                <hr>
                <li>
                    <a href="/BukuGlassFish/pengarang">
                        <img class="icon-menu" src="asset/images/icon/petugas.png" alt="none">
                        <span>Pengarang</span>
                    </a>
                </li>
                <hr>
            </ul>
            <br>
            <br>
            <img id="button" class="panah" src="asset/images/icon/kiri.png" alt="none">
        </aside>
    </nav>
    <section>
        <article id="isi" class="isi">
            <div class="grid-container">
                <div class="baris">
                    <div class="kolom">
                        <div class="box">
                            <div class="box-container judul">
                                <h4><b>Halaman Judul</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-container">
                <div class="baris">
                    <div class="kolom">
                        <div class="box">
                            <div class="box-container">
                                <div class="baris">
                                    <div class="kolom">
                                        <div class="box-native">
                                            <div class="box-container">
                                                <i onclick="tambahBuku()" class="fa fa-book fa-4x" aria-hidden="true"></i><br>
                                                Buku
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kolom">
                                        <div class="box-native">
                                            <div class="box-container">
                                                <i onclick="tambahPengarang()"class="fa fa-user-secret fa-4x" aria-hidden="true"></i><br>
                                                Pengarang
                                            </div>
                                        </div>
                                    </div> 
                                        <div class="kolom">
                                        <div class="box-native">
                                            <div class="box-container">
                                                <i onclick="tambahTransaksi()" class="fa fa-file-text-o  fa-4x" aria-hidden="true"></i><br>
                                                Transaksi
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        
        <div id="tambahbuku" class="w3-modal">
          <div class="w3-modal-content w3-animate-left w3-card-4">
            <header class="w3-container w3-teal"> 
              <span onclick="document.getElementById('tambahbuku').style.display='none'" 
              class="w3-button w3-display-topright">&times;</span>
              <h2 style="text-align: center;">Form Tambah Data</h2>
            </header>
            <div style="text-align: center;" class="w3-container" id="tambahBukuModal">
            </div>
            <footer class="w3-container w3-teal">
              <p>Modal Footer</p>
            </footer>
          </div>
        </div>
        <div id="tambahpengarang" class="w3-modal">
          <div class="w3-modal-content w3-animate-left w3-card-4">
            <header class="w3-container w3-teal"> 
              <span onclick="document.getElementById('tambahpengarang').style.display='none'" 
              class="w3-button w3-display-topright">&times;</span>
              <h2 style="text-align: center;">Form Tambah Data</h2>
            </header>
            <div style="text-align: center;" class="w3-container" id="tambahPengarangModal">
            </div>
            <footer class="w3-container w3-teal">
              <p>Modal Footer</p>
            </footer>
          </div>
        </div>
        <div id="tambahtransaksi" class="w3-modal">
            <form action="/BukuGlassFish/transaksi?post=tambah" method="post">
            <div class="w3-modal-content w3-animate-left w3-card-4" style="width:900px;">
            <header class="w3-container w3-teal"> 
              <span onclick="document.getElementById('tambahtransaksi').style.display='none'" 
              class="w3-button w3-display-topright">&times;</span>
              <select id="pilih" style="width:100px;float: left;padding: 5px;position: absolute;margin-top: 25px;">
                  <option value="buku">Buku</option>
                  <option value="pengarang">Pengarang</option>
              </select>
              <h2 style="text-align: center;">Form Tambah Data</h2>
            </header>
            <div style="text-align: center;" class="w3-container" id="tambahTransaksiModal">
            <div id="A" style="width:48%; display:inline-block; float: left; margin-right: 10px;"></div>
            <div id="B" style="width:48%; display:inline-block; float: right; margin-left: 10px;"></div>
            </div>
            <footer  class="w3-container w3-teal">
                <div id="foot"></div>
            </footer>
          </div>
            </form>
        </div>
    </section>
    <footer>
        <div id="foot" class="foot">
            <div class="identitas">
                <strong>Wahyu Setiawan || 17.5.00173</strong>
            </div>
        </div>
    </footer>

    <script type="text/javascript" src="asset/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="asset/js/sidebar.js"></script>    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        var xno = 0;
        $(function(){
            $("#pilih").on('change', function(){
                console.log($(this).val() );
              if($(this).val() == 'buku'){
                  tambahTransaksi();
              }else if($(this).val() == 'pengarang'){
                  tambahTransaksi2();
              }
            })

         });
        function tambahTransaksi(){
            xno = 0;
            document.getElementById('A').innerHTML = ""
            document.getElementById('B').innerHTML = ""
            document.getElementById('foot').innerHTML = ""
            document.getElementById('tambahtransaksi').style.display='block';
                
                var f = document.createElement("div");
                
                var para = document.createElement("h4");
                var node = document.createTextNode("Data Buku");
                para.appendChild(node)
                
                var a = document.createElement("select"); //input element, Submit button
                a.setAttribute('name',"no");
                <%
                    BukuImplDAO Buku=new BukuImplDAO();
                    List<BukuModel> buku= (ArrayList) Buku.getAllBuku();
                    for(BukuModel bk : buku){
                %>
                    var option = document.createElement('option');
                    option.text = "<%out.print(bk.getNamaBuku());%>";
                    option.value = "<%out.print(bk.getNoBuku());%>";
                    a.appendChild(option)
                <%}%>
                var b = document.createElement("input"); //input element, Submit button
                b.setAttribute('type',"text");
                b.setAttribute('name',"namapenerbit");
                b.setAttribute('placeholder',"Nama Penerbit");
                
                var bb = document.createElement("input"); //input element, Submit button
                bb.setAttribute('type',"text");
                bb.setAttribute('name',"alamatpenerbit");
                bb.setAttribute('placeholder',"Alamat Penerbit");

                var m = document.createElement("input"); //input element, Submit button
                m.setAttribute('type',"hidden");
                m.setAttribute('name',"mode");
                m.setAttribute('value',"buku");

                f.appendChild(para);
                f.appendChild(m);
                f.appendChild(a);
                f.appendChild(b);
                f.appendChild(bb);
                document.getElementById('A').appendChild(f);
                
                var g = document.createElement("div");
                
                var para1 = document.createElement("h4");
                var node1 = document.createTextNode("Daftar Pengarang");
                para1.appendChild(node1)
                                
                var ac = document.createElement("select"); //input element, Submit button
                var nos = 'noo['+xno+']';
                ac.setAttribute('name',nos);
                <%
                    PengarangImplDAO Pengarang=new PengarangImplDAO();
                    List<PengarangModel> pengarang= (ArrayList) Pengarang.getAllPengarang();
                    for(PengarangModel pg : pengarang){
                %>
                    var option = document.createElement('option');
                    option.text = "<%out.print(pg.getNamaPengarang());%>";
                    option.value = "<%out.print(pg.getNoPengarang());%>";
                    ac.appendChild(option)
                <%}%>
                
                var div = document.createElement("div");
                div.setAttribute('id','pilihan');
                div.appendChild(ac)
                
                var btn = document.createElement("i");
                btn.setAttribute('class','fa fa-plus fa-2x');
                btn.setAttribute('onclick','addPilihan()');
                
                g.appendChild(para1);
                g.appendChild(div);
                g.appendChild(btn);
                document.getElementById('B').appendChild(g);
                
                var ff = document.createElement("div");
                
                var s = document.createElement("input"); //input element, Submit button
                s.setAttribute('type',"submit");
                s.setAttribute('value',"Submit");
                ff.appendChild(s);
                document.getElementById('foot').appendChild(ff);

                $('#tambahTransaksiBaru').submit(function(e) {

                    e.preventDefault(); // avoid to execute the actual submit of the form.

                    var form = $(this);
                    var url = form.attr('action');

                    $.ajax({
                           type: "POST",
                           url: url,
                           data: form.serialize(), // serializes the form's elements.
//                           data: $('#tambahTransaksiBaru, #tambahTransaksiBaru1, #tambahTransaksiBaru2').serialize(), // serializes the form's elements.
                           success: function(data)
                           {
                               console.log("got response as "+data);
                                if(data['kode'] == 200){
                                swal("Transaksi di Tambahkan !!!", "Transaksi telah berhasil di tambah !", "success").then(function(){
                                    window.location.href = "http://localhost:8080/BukuGlassFish/transaksi?sort=buku";
                                });
                                }
                           }
                         });
                });
            xno = 1;
        }
        
        function addPilihan(){
            var ac = document.createElement("select"); //input element, Submit button
                ac.setAttribute('name',"noo["+xno+"]");
                <%
                    List<PengarangModel> peng= (ArrayList) Pengarang.getAllPengarang();
                    for(PengarangModel pg : peng){
                %>
                    var option = document.createElement('option');
                    option.text = "<%out.print(pg.getNamaPengarang());%>";
                    option.value = "<%out.print(pg.getNoPengarang());%>";
                    ac.appendChild(option)
                <%}%>
            document.getElementById('pilihan').appendChild(ac);
            xno++;
        }
        
        function tambahTransaksi2(){
            xno = 0;
            document.getElementById('A').innerHTML = ""
            document.getElementById('B').innerHTML = ""
            document.getElementById('foot').innerHTML = ""
            document.getElementById('tambahtransaksi').style.display='block';
                
                var f = document.createElement("div");
                
                var para = document.createElement("h4");
                var node = document.createTextNode("Data Pengarang");
                para.appendChild(node)
                
                var a = document.createElement("select"); //input element, Submit button
                a.setAttribute('name',"no");
                <%
                    PengarangImplDAO Pengarangg=new PengarangImplDAO();
                    List<PengarangModel> pengarangg= (ArrayList) Pengarangg.getAllPengarang();
                    for(PengarangModel pg : pengarangg){
                %>
                    var option = document.createElement('option');
                    option.text = "<%out.print(pg.getNamaPengarang());%>";
                    option.value = "<%out.print(pg.getNoPengarang());%>";
                    a.appendChild(option)
                <%}%>
                var b = document.createElement("input"); //input element, Submit button
                b.setAttribute('type',"text");
                b.setAttribute('name',"namapenerbit");
                b.setAttribute('placeholder',"Nama Penerbit");
                
                var bb = document.createElement("input"); //input element, Submit button
                bb.setAttribute('type',"text");
                bb.setAttribute('name',"alamatpenerbit");
                bb.setAttribute('placeholder',"Alamat Penerbit");

                var m = document.createElement("input"); //input element, Submit button
                m.setAttribute('type',"hidden");
                m.setAttribute('name',"mode");
                m.setAttribute('value',"pengarang");

                f.appendChild(para);
                f.appendChild(m);
                f.appendChild(a);
                f.appendChild(b);
                f.appendChild(bb);
                document.getElementById('A').appendChild(f);
                
                var g = document.createElement("div");
                
                var para1 = document.createElement("h4");
                var node1 = document.createTextNode("Daftar Buku");
                para1.appendChild(node1)
                                
                var ac = document.createElement("select"); //input element, Submit button
                var nos = 'noo['+xno+']';
                ac.setAttribute('name',nos);
                <%
                    BukuImplDAO Bukuu=new BukuImplDAO();
                    List<BukuModel> bukuu= (ArrayList) Bukuu.getAllBuku();
                    for(BukuModel bk : bukuu){
                %>
                    var option = document.createElement('option');
                    option.text = "<%out.print(bk.getNamaBuku());%>";
                    option.value = "<%out.print(bk.getNoBuku());%>";
                    ac.appendChild(option)
                <%}%>
                
                var div = document.createElement("div");
                div.setAttribute('id','pilihan');
                div.appendChild(ac)
                
                var btn = document.createElement("i");
                btn.setAttribute('class','fa fa-plus fa-2x');
                btn.setAttribute('onclick','addPilihan2()');
                
                g.appendChild(para1);
                g.appendChild(div);
                g.appendChild(btn);
                document.getElementById('B').appendChild(g);
                
                var ff = document.createElement("div");
                
                var s = document.createElement("input"); //input element, Submit button
                s.setAttribute('type',"submit");
                s.setAttribute('value',"Submit");
                ff.appendChild(s);
                document.getElementById('foot').appendChild(ff);

                $('#tambahTransaksiBaru').submit(function(e) {

                    e.preventDefault(); // avoid to execute the actual submit of the form.

                    var form = $(this);
                    var url = form.attr('action');

                    $.ajax({
                           type: "POST",
                           url: url,
                           data: form.serialize(), // serializes the form's elements.
//                           data: $('#tambahTransaksiBaru, #tambahTransaksiBaru1, #tambahTransaksiBaru2').serialize(), // serializes the form's elements.
                           success: function(data)
                           {
                               console.log("got response as "+data);
                                if(data['kode'] == 200){
                                swal("Transaksi di Tambahkan !!!", "Transaksi telah berhasil di tambah !", "success").then(function(){
                                    window.location.href = "http://localhost:8080/BukuGlassFish/transaksi?sort=buku";
                                });
                                }
                           }
                         });
                });
            xno = 1;
        }
        
        function addPilihan2(){
            var ac = document.createElement("select"); //input element, Submit button
                ac.setAttribute('name',"noo["+xno+"]");
                <%
                    List<BukuModel> bkk= (ArrayList) Bukuu.getAllBuku();
                    for(BukuModel bkkk : bkk){
                %>
                    var option = document.createElement('option');
                    option.text = "<%out.print(bkkk.getNamaBuku());%>";
                    option.value = "<%out.print(bkkk.getNoBuku());%>";
                    ac.appendChild(option)
                <%}%>
            document.getElementById('pilihan').appendChild(ac);
            xno++;
        }
        
         function tambahBuku(){
            document.getElementById('tambahBukuModal').innerHTML = ""
            document.getElementById('tambahbuku').style.display='block';
            var f = document.createElement("form");
                f.setAttribute('method',"post");
                f.setAttribute('id',"tambahBukuBaru");
                f.setAttribute('action',"/BukuGlassFish/buku?post=tambah");

                var para = document.createElement("p");
                var node = document.createTextNode("Tambah Data Buku !");
                para.appendChild(node)

                var a = document.createElement("input"); //input element, Submit button
                a.setAttribute('type',"text");
                a.setAttribute('name',"namabuku");
                a.setAttribute('placeholder',"Nama Buku");

                var b = document.createElement("input"); //input element, Submit button
                b.setAttribute('type',"text");
                b.setAttribute('name',"isbnbuku");
                b.setAttribute('placeholder',"No ISBN Buku");

                var c = document.createElement("input"); //input element, Submit button
                c.setAttribute('type',"date");
                c.setAttribute('name',"tglterbit");
                c.setAttribute('placeholder',"Tanggal Terbit Buku");

                var s = document.createElement("input"); //input element, Submit button
                s.setAttribute('type',"submit");
                s.setAttribute('value',"Submit");
                f.appendChild(para);
                f.appendChild(a);
                f.appendChild(b);
                f.appendChild(c);
                f.appendChild(s);

                document.getElementById('tambahBukuModal').appendChild(f);
                $('#tambahBukuBaru').submit(function(e) {

                    e.preventDefault(); // avoid to execute the actual submit of the form.
                    var form = $(this);
                    var url = form.attr('action');

                    $.ajax({
                           type: "POST",
                           url: url,
                           data: form.serialize(), // serializes the form's elements.
                           success: function(data)
                           {
                               console.log("got response as "+data);
                                if(data['kode'] == 200){
                                swal("Buku di Tambahkan !!!", "Buku telah berhasil di tambah !", "success").then(function(){
                                    window.location.href = "http://localhost:8080/BukuGlassFish/buku";
                                });
                                }
                           }
                         });


                });
        }
        
        function tambahPengarang(){
            document.getElementById('tambahPengarangModal').innerHTML = ""
            document.getElementById('tambahpengarang').style.display='block';
            var f = document.createElement("form");
                f.setAttribute('method',"post");
                f.setAttribute('id',"tambahPengarangBaru");
                f.setAttribute('action',"/BukuGlassFish/pengarang?post=tambah");

                var para = document.createElement("p");
                var node = document.createTextNode("Tambah Data Pengarang !");
                para.appendChild(node)

                var a = document.createElement("input"); //input element, Submit button
                a.setAttribute('type',"text");
                a.setAttribute('name',"namapengarang");
                a.setAttribute('placeholder',"Nama Pengarang");

                var b = document.createElement("input"); //input element, Submit button
                b.setAttribute('type',"text");
                b.setAttribute('name',"alamatpengarang");
                b.setAttribute('placeholder',"Alamat Pengarang");

                var s = document.createElement("input"); //input element, Submit button
                s.setAttribute('type',"submit");
                s.setAttribute('value',"Submit");
                f.appendChild(para);
                f.appendChild(a);
                f.appendChild(b);
                f.appendChild(s);

                document.getElementById('tambahPengarangModal').appendChild(f);
                $('#tambahPengarangBaru').submit(function(e) {

                    e.preventDefault(); // avoid to execute the actual submit of the form.
                    var form = $(this);
                    var url = form.attr('action');

                    $.ajax({
                           type: "POST",
                           url: url,
                           data: form.serialize(), // serializes the form's elements.
                           success: function(data)
                           {
                               console.log("got response as "+data);
                                if(data['kode'] == 200){
                                swal("Pengarang di Tambahkan !!!", "Pengarang telah berhasil di tambah !", "success").then(function(){
                                    window.location.href = "http://localhost:8080/BukuGlassFish/pengarang";
                                });
                                }
                           }
                         });


                });
        }
        
    </script>
</body>

</html>
