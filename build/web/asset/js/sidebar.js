var sidehide = false;
$('#button').on('click', function () {
    if(sidehide){
        $('#menu').css("width", "15%");
        $('#isi').css("width", "83%");
        $('#foot').css("width", "83%");
        $("#head-img").attr("src","asset/images/wordmark4.png");
        $("#menu span").show();
        $('#button').attr("src","asset/images/icon/kiri.png")
        sidehide = false;
    }else{
        $('#menu').css("width", "4%");
        $('#isi').css("width", "94%");
        $('#foot').css("width", "94%");
        $("#head-img").attr("src","asset/images/logo.png");
        $("#menu span").hide();
        $('#button').attr("src","asset/images/icon/kanan.png")
        sidehide = true;
    }
})